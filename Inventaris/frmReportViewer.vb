﻿Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Data

Public Class frmReportViewer

    Public param(3) As String
    Public filter As String
    Public filename As String
    Public formula As String
    Private Sub frmReportViewer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim Report As New ReportDocument
        Dim Report1 As New ReportDocument
        Dim li As New TableLogOnInfo
        Dim tbs As Tables
        Dim tb As Table
        Try

            Report.Load(reportLocation & filename)

            li.ConnectionInfo.DatabaseName = dbName
            li.ConnectionInfo.UserID = loginSA
            li.ConnectionInfo.Password = passwordSA
            li.ConnectionInfo.ServerName = serverName

            tbs = Report.Database.Tables
            For Each tb In Report.Database.Tables
                tb.ApplyLogOnInfo(li)
            Next

            'Report.SetDatabaseLogon(user, pwd, servername, dbname)

            Report.RecordSelectionFormula = formula
            Report.Refresh()
            Report.SetParameterValue(0, param(0))
            Report.SetParameterValue(1, param(1))
            Report.SetParameterValue(2, param(2))

            crV.ReportSource = Report
            crV.Show()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class