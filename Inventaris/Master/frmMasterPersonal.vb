﻿Public Class frmMasterPersonal

    Private Sub btnSearchPersonal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchPersonal.Click
        With frmCari
            .tabel = "ms_personal_instansi"
            .kolom = "*"
            .kondisi = ""
            .urut = ""

            .col_name = "kode_personal_instansi"

            If .ShowDialog = Windows.Forms.DialogResult.OK Then
                txtKode.Text = .hasil
                load_dataPersonal()
            End If
        End With
    End Sub

    Private Sub load_dataPersonal()
        Try
            conn.Open()

            cmd = New SqlCommand("select * from ms_personal_instansi where kode_personal_instansi = '" & txtKode.Text & "'", conn)
            reader = cmd.ExecuteReader
            If reader.Read Then
                txtNama.Text = reader!nama_personal_instansi
                txtTelp.Text = reader!telp
                txtAlamat.Text = reader!alamat
                txtCP.Text = reader!cp
                txtTelpCP.Text = reader!telp_cp
                If reader!isPersonal Then rbPersonal.Checked = True Else rbInstansi.Checked = True
                chkSupplier.Checked = reader!isSupplier
                chkCustomer.Checked = reader!isCustomer
                chkKaryawan.Checked = reader!isKaryawan
                chkAktif.Checked = reader!isAktif
            End If
            reader.Close()
            conn.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
            If conn.State Then conn.Close()
        End Try
    End Sub

    Private Sub frmMasterPersonal_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F5 Then btnSearchPersonal.PerformClick()
        If e.KeyCode = Keys.F2 Then btnSimpan.PerformClick()
        If e.KeyCode = Keys.Escape Then Me.Close()
    End Sub

    Private Sub frmMasterPersonal_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnBaru_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBaru.Click
        reset_form()
    End Sub

    Private Sub reset_form()
        txtKode.Text = ""
        txtNama.Text = ""
        chkSupplier.Checked = False
        chkCustomer.Checked = False
        chkKaryawan.Checked = False
        txtTelp.Text = ""
        txtAlamat.Text = ""
        txtCP.Text = ""
        txtTelpCP.Text = ""
        chkAktif.Checked = False
    End Sub

    Private Sub btnSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSimpan.Click
        If txtNama.Text = "" Then
            MsgBox("Nama Personal/Instansi tidak boleh kosong")
            Exit Sub
        End If

        Dim inTrans As Boolean
        Dim transaction As SqlTransaction = Nothing
        Try
            Dim query As String
            conn.Open()
            transaction = conn.BeginTransaction(IsolationLevel.ReadCommitted)
            inTrans = True
            If txtKode.Text = "" Then
                txtKode.Text = newIdUrut("ms_personal_instansi", "kode_personal_instansi", IIf(rbPersonal.Checked, "PERS", "INST"), 3)
                query = add_dataPersonalInstansi(True)
            Else
                query = add_dataPersonalInstansi(False)
            End If
            cmd = New SqlCommand(query, conn, transaction)
            cmd.ExecuteNonQuery()

            transaction.Commit()
            inTrans = False
            If conn.State Then conn.Close()
            MsgBox("Data telah tersimpan")
            reset_form()
        Catch ex As Exception
            If inTrans Then transaction.Rollback()
            If conn.State Then conn.Close()
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Function add_dataPersonalInstansi(ByVal isNew As Boolean)
        Dim nama_tabel As String
        Dim kolom() As String
        Dim data() As String

        ReDim kolom(11)
        ReDim data(11)

        nama_tabel = "ms_personal_instansi"
        kolom(0) = "kode_personal_instansi"
        kolom(1) = "nama_personal_instansi"
        kolom(2) = "isPersonal"
        kolom(3) = "isSupplier"
        kolom(4) = "isCustomer"
        kolom(5) = "isKaryawan"
        kolom(6) = "telp"
        kolom(7) = "alamat"
        kolom(8) = "cp"
        kolom(9) = "telp_cp"
        kolom(10) = "isAktif"

        data(0) = txtKode.Text
        data(1) = txtNama.Text
        data(2) = rbPersonal.Checked
        data(3) = chkSupplier.Checked
        data(4) = chkCustomer.Checked
        data(5) = chkKaryawan.Checked
        data(6) = txtTelp.Text
        data(7) = txtAlamat.Text
        data(8) = txtCP.Text
        data(9) = txtTelpCP.Text
        data(10) = chkAktif.Checked

        If isNew Then
            Return tambah_data(nama_tabel, kolom, data)
        Else
            Return update_data(nama_tabel, kolom, data, "kode_personal_instansi = '" & txtKode.Text & "'")
        End If
    End Function

    Private Sub chkSupplier_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSupplier.CheckedChanged
        'If chkSupplier.Checked Or chkCustomer.Checked Then chkKaryawan.Checked = False
    End Sub

    Private Sub chkCustomer_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCustomer.CheckedChanged
        'If chkSupplier.Checked Or chkCustomer.Checked Then chkKaryawan.Checked = False
    End Sub

    Private Sub chkKaryawan_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkKaryawan.CheckedChanged
        'If chkKaryawan.Checked Then
        '    chkSupplier.Checked = False
        '    chkCustomer.Checked = False
        'End If
    End Sub

    Private Sub btnHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHapus.Click
        Try
            conn.Open()
            cmd = New SqlCommand("delete from ms_personal_instansi where kode_personal_instansi = '" & txtKode.Text & "'", conn)
            cmd.ExecuteNonQuery()
            conn.Close()
            MsgBox("Data sudah dihapus")
            reset_form()
        Catch ex As Exception
            MsgBox(ex.Message)
            If conn.State Then conn.Close()
        End Try
    End Sub
End Class