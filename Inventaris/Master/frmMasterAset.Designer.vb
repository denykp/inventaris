﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMasterAset
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtKodeAset = New System.Windows.Forms.TextBox()
        Me.txtNamaAset = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cmbMerk = New System.Windows.Forms.ComboBox()
        Me.cmbKategori = New System.Windows.Forms.ComboBox()
        Me.pbAset = New System.Windows.Forms.PictureBox()
        Me.txtDirectoryAset = New System.Windows.Forms.TextBox()
        Me.btnSearchAset = New System.Windows.Forms.Button()
        Me.cmdSearchPicture = New System.Windows.Forms.Button()
        Me.btnSimpan = New System.Windows.Forms.Button()
        Me.bntHapus = New System.Windows.Forms.Button()
        Me.btnBaru = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtCatatan = New System.Windows.Forms.TextBox()
        Me.chkAktif = New System.Windows.Forms.CheckBox()
        CType(Me.pbAset, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Kode Aset"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(109, 14)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(10, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = ":"
        '
        'txtKodeAset
        '
        Me.txtKodeAset.Location = New System.Drawing.Point(125, 11)
        Me.txtKodeAset.Name = "txtKodeAset"
        Me.txtKodeAset.ReadOnly = True
        Me.txtKodeAset.Size = New System.Drawing.Size(109, 20)
        Me.txtKodeAset.TabIndex = 0
        '
        'txtNamaAset
        '
        Me.txtNamaAset.Location = New System.Drawing.Point(125, 37)
        Me.txtNamaAset.Name = "txtNamaAset"
        Me.txtNamaAset.Size = New System.Drawing.Size(243, 20)
        Me.txtNamaAset.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(109, 40)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(10, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = ":"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(12, 40)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(59, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Nama Aset"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(109, 66)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(10, 13)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = ":"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 66)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(31, 13)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "Merk"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(109, 92)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(10, 13)
        Me.Label7.TabIndex = 10
        Me.Label7.Text = ":"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(12, 92)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(46, 13)
        Me.Label8.TabIndex = 9
        Me.Label8.Text = "Kategori"
        '
        'cmbMerk
        '
        Me.cmbMerk.FormattingEnabled = True
        Me.cmbMerk.Location = New System.Drawing.Point(125, 63)
        Me.cmbMerk.Name = "cmbMerk"
        Me.cmbMerk.Size = New System.Drawing.Size(109, 21)
        Me.cmbMerk.TabIndex = 2
        '
        'cmbKategori
        '
        Me.cmbKategori.FormattingEnabled = True
        Me.cmbKategori.Location = New System.Drawing.Point(125, 89)
        Me.cmbKategori.Name = "cmbKategori"
        Me.cmbKategori.Size = New System.Drawing.Size(109, 21)
        Me.cmbKategori.TabIndex = 3
        '
        'pbAset
        '
        Me.pbAset.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pbAset.Location = New System.Drawing.Point(400, 37)
        Me.pbAset.Name = "pbAset"
        Me.pbAset.Size = New System.Drawing.Size(236, 162)
        Me.pbAset.TabIndex = 13
        Me.pbAset.TabStop = False
        '
        'txtDirectoryAset
        '
        Me.txtDirectoryAset.Location = New System.Drawing.Point(400, 11)
        Me.txtDirectoryAset.Name = "txtDirectoryAset"
        Me.txtDirectoryAset.Size = New System.Drawing.Size(155, 20)
        Me.txtDirectoryAset.TabIndex = 5
        '
        'btnSearchAset
        '
        Me.btnSearchAset.Location = New System.Drawing.Point(240, 9)
        Me.btnSearchAset.Name = "btnSearchAset"
        Me.btnSearchAset.Size = New System.Drawing.Size(75, 23)
        Me.btnSearchAset.TabIndex = 0
        Me.btnSearchAset.Text = "Search (F5)"
        Me.btnSearchAset.UseVisualStyleBackColor = True
        '
        'cmdSearchPicture
        '
        Me.cmdSearchPicture.Location = New System.Drawing.Point(561, 9)
        Me.cmdSearchPicture.Name = "cmdSearchPicture"
        Me.cmdSearchPicture.Size = New System.Drawing.Size(75, 23)
        Me.cmdSearchPicture.TabIndex = 6
        Me.cmdSearchPicture.Text = "Browse"
        Me.cmdSearchPicture.UseVisualStyleBackColor = True
        '
        'btnSimpan
        '
        Me.btnSimpan.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnSimpan.Location = New System.Drawing.Point(287, 248)
        Me.btnSimpan.Name = "btnSimpan"
        Me.btnSimpan.Size = New System.Drawing.Size(75, 23)
        Me.btnSimpan.TabIndex = 8
        Me.btnSimpan.Text = "Simpan (F2)"
        Me.btnSimpan.UseVisualStyleBackColor = True
        '
        'bntHapus
        '
        Me.bntHapus.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.bntHapus.Location = New System.Drawing.Point(368, 248)
        Me.bntHapus.Name = "bntHapus"
        Me.bntHapus.Size = New System.Drawing.Size(75, 23)
        Me.bntHapus.TabIndex = 9
        Me.bntHapus.Text = "Hapus"
        Me.bntHapus.UseVisualStyleBackColor = True
        '
        'btnBaru
        '
        Me.btnBaru.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnBaru.Location = New System.Drawing.Point(206, 248)
        Me.btnBaru.Name = "btnBaru"
        Me.btnBaru.Size = New System.Drawing.Size(75, 23)
        Me.btnBaru.TabIndex = 7
        Me.btnBaru.Text = "Baru"
        Me.btnBaru.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(109, 119)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(10, 13)
        Me.Label9.TabIndex = 22
        Me.Label9.Text = ":"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(12, 119)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(44, 13)
        Me.Label10.TabIndex = 21
        Me.Label10.Text = "Catatan"
        '
        'txtCatatan
        '
        Me.txtCatatan.Location = New System.Drawing.Point(125, 116)
        Me.txtCatatan.Multiline = True
        Me.txtCatatan.Name = "txtCatatan"
        Me.txtCatatan.Size = New System.Drawing.Size(243, 83)
        Me.txtCatatan.TabIndex = 4
        '
        'chkAktif
        '
        Me.chkAktif.AutoSize = True
        Me.chkAktif.Location = New System.Drawing.Point(321, 13)
        Me.chkAktif.Name = "chkAktif"
        Me.chkAktif.Size = New System.Drawing.Size(47, 17)
        Me.chkAktif.TabIndex = 44
        Me.chkAktif.Text = "Aktif"
        Me.chkAktif.UseVisualStyleBackColor = True
        '
        'frmMasterAset
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(648, 283)
        Me.Controls.Add(Me.chkAktif)
        Me.Controls.Add(Me.txtCatatan)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.btnBaru)
        Me.Controls.Add(Me.bntHapus)
        Me.Controls.Add(Me.btnSimpan)
        Me.Controls.Add(Me.cmdSearchPicture)
        Me.Controls.Add(Me.btnSearchAset)
        Me.Controls.Add(Me.txtDirectoryAset)
        Me.Controls.Add(Me.pbAset)
        Me.Controls.Add(Me.cmbKategori)
        Me.Controls.Add(Me.cmbMerk)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtNamaAset)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtKodeAset)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.Name = "frmMasterAset"
        Me.Text = "Master Aset"
        CType(Me.pbAset, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtKodeAset As System.Windows.Forms.TextBox
    Friend WithEvents txtNamaAset As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cmbMerk As System.Windows.Forms.ComboBox
    Friend WithEvents cmbKategori As System.Windows.Forms.ComboBox
    Friend WithEvents pbAset As System.Windows.Forms.PictureBox
    Friend WithEvents txtDirectoryAset As System.Windows.Forms.TextBox
    Friend WithEvents btnSearchAset As System.Windows.Forms.Button
    Friend WithEvents cmdSearchPicture As System.Windows.Forms.Button
    Friend WithEvents btnSimpan As System.Windows.Forms.Button
    Friend WithEvents bntHapus As System.Windows.Forms.Button
    Friend WithEvents btnBaru As System.Windows.Forms.Button
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtCatatan As System.Windows.Forms.TextBox
    Friend WithEvents chkAktif As System.Windows.Forms.CheckBox
End Class
