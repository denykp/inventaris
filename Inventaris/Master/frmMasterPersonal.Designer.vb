﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMasterPersonal
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnSearchPersonal = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtNama = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtKode = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtTelp = New System.Windows.Forms.TextBox()
        Me.txtAlamat = New System.Windows.Forms.TextBox()
        Me.btnBaru = New System.Windows.Forms.Button()
        Me.btnHapus = New System.Windows.Forms.Button()
        Me.btnSimpan = New System.Windows.Forms.Button()
        Me.gbJenisPersonal = New System.Windows.Forms.GroupBox()
        Me.rbInstansi = New System.Windows.Forms.RadioButton()
        Me.rbPersonal = New System.Windows.Forms.RadioButton()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtTelpCP = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtCP = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.chkSupplier = New System.Windows.Forms.CheckBox()
        Me.chkCustomer = New System.Windows.Forms.CheckBox()
        Me.chkKaryawan = New System.Windows.Forms.CheckBox()
        Me.chkAktif = New System.Windows.Forms.CheckBox()
        Me.gbJenisPersonal.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnSearchPersonal
        '
        Me.btnSearchPersonal.Location = New System.Drawing.Point(240, 41)
        Me.btnSearchPersonal.Name = "btnSearchPersonal"
        Me.btnSearchPersonal.Size = New System.Drawing.Size(75, 23)
        Me.btnSearchPersonal.TabIndex = 0
        Me.btnSearchPersonal.Text = "Search (F5)"
        Me.btnSearchPersonal.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(109, 147)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(10, 13)
        Me.Label7.TabIndex = 25
        Me.Label7.Text = ":"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(12, 147)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(39, 13)
        Me.Label8.TabIndex = 24
        Me.Label8.Text = "Alamat"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(109, 121)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(10, 13)
        Me.Label5.TabIndex = 23
        Me.Label5.Text = ":"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 121)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(28, 13)
        Me.Label6.TabIndex = 22
        Me.Label6.Text = "Telp"
        '
        'txtNama
        '
        Me.txtNama.Location = New System.Drawing.Point(125, 69)
        Me.txtNama.Name = "txtNama"
        Me.txtNama.Size = New System.Drawing.Size(243, 20)
        Me.txtNama.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(109, 72)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(10, 13)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = ":"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(12, 72)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(35, 13)
        Me.Label4.TabIndex = 19
        Me.Label4.Text = "Nama"
        '
        'txtKode
        '
        Me.txtKode.Location = New System.Drawing.Point(125, 43)
        Me.txtKode.Name = "txtKode"
        Me.txtKode.ReadOnly = True
        Me.txtKode.Size = New System.Drawing.Size(109, 20)
        Me.txtKode.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(109, 46)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(10, 13)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = ":"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 46)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 13)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Kode"
        '
        'txtTelp
        '
        Me.txtTelp.Location = New System.Drawing.Point(125, 118)
        Me.txtTelp.Name = "txtTelp"
        Me.txtTelp.Size = New System.Drawing.Size(242, 20)
        Me.txtTelp.TabIndex = 5
        '
        'txtAlamat
        '
        Me.txtAlamat.Location = New System.Drawing.Point(125, 144)
        Me.txtAlamat.Multiline = True
        Me.txtAlamat.Name = "txtAlamat"
        Me.txtAlamat.Size = New System.Drawing.Size(243, 89)
        Me.txtAlamat.TabIndex = 6
        '
        'btnBaru
        '
        Me.btnBaru.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnBaru.Location = New System.Drawing.Point(73, 354)
        Me.btnBaru.Name = "btnBaru"
        Me.btnBaru.Size = New System.Drawing.Size(75, 23)
        Me.btnBaru.TabIndex = 9
        Me.btnBaru.Text = "Baru"
        Me.btnBaru.UseVisualStyleBackColor = True
        '
        'btnHapus
        '
        Me.btnHapus.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnHapus.Location = New System.Drawing.Point(235, 354)
        Me.btnHapus.Name = "btnHapus"
        Me.btnHapus.Size = New System.Drawing.Size(75, 23)
        Me.btnHapus.TabIndex = 11
        Me.btnHapus.Text = "Hapus"
        Me.btnHapus.UseVisualStyleBackColor = True
        '
        'btnSimpan
        '
        Me.btnSimpan.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnSimpan.Location = New System.Drawing.Point(154, 354)
        Me.btnSimpan.Name = "btnSimpan"
        Me.btnSimpan.Size = New System.Drawing.Size(75, 23)
        Me.btnSimpan.TabIndex = 10
        Me.btnSimpan.Text = "Simpan (F2)"
        Me.btnSimpan.UseVisualStyleBackColor = True
        '
        'gbJenisPersonal
        '
        Me.gbJenisPersonal.Controls.Add(Me.rbInstansi)
        Me.gbJenisPersonal.Controls.Add(Me.rbPersonal)
        Me.gbJenisPersonal.Location = New System.Drawing.Point(125, 4)
        Me.gbJenisPersonal.Name = "gbJenisPersonal"
        Me.gbJenisPersonal.Size = New System.Drawing.Size(202, 33)
        Me.gbJenisPersonal.TabIndex = 35
        Me.gbJenisPersonal.TabStop = False
        '
        'rbInstansi
        '
        Me.rbInstansi.AutoSize = True
        Me.rbInstansi.Location = New System.Drawing.Point(102, 10)
        Me.rbInstansi.Name = "rbInstansi"
        Me.rbInstansi.Size = New System.Drawing.Size(61, 17)
        Me.rbInstansi.TabIndex = 1
        Me.rbInstansi.TabStop = True
        Me.rbInstansi.Text = "Instansi"
        Me.rbInstansi.UseVisualStyleBackColor = True
        '
        'rbPersonal
        '
        Me.rbPersonal.AutoSize = True
        Me.rbPersonal.Checked = True
        Me.rbPersonal.Location = New System.Drawing.Point(6, 10)
        Me.rbPersonal.Name = "rbPersonal"
        Me.rbPersonal.Size = New System.Drawing.Size(66, 17)
        Me.rbPersonal.TabIndex = 0
        Me.rbPersonal.TabStop = True
        Me.rbPersonal.Text = "Personal"
        Me.rbPersonal.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(12, 96)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(29, 13)
        Me.Label9.TabIndex = 36
        Me.Label9.Text = "Role"
        '
        'txtTelpCP
        '
        Me.txtTelpCP.Location = New System.Drawing.Point(125, 265)
        Me.txtTelpCP.Name = "txtTelpCP"
        Me.txtTelpCP.Size = New System.Drawing.Size(242, 20)
        Me.txtTelpCP.TabIndex = 8
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(109, 268)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(10, 13)
        Me.Label10.TabIndex = 42
        Me.Label10.Text = ":"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(12, 268)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(45, 13)
        Me.Label11.TabIndex = 41
        Me.Label11.Text = "Telp CP"
        '
        'txtCP
        '
        Me.txtCP.Location = New System.Drawing.Point(125, 239)
        Me.txtCP.Name = "txtCP"
        Me.txtCP.Size = New System.Drawing.Size(242, 20)
        Me.txtCP.TabIndex = 7
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(109, 242)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(10, 13)
        Me.Label12.TabIndex = 39
        Me.Label12.Text = ":"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(12, 242)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(80, 13)
        Me.Label13.TabIndex = 38
        Me.Label13.Text = "Contact Person"
        '
        'chkSupplier
        '
        Me.chkSupplier.Location = New System.Drawing.Point(125, 95)
        Me.chkSupplier.Name = "chkSupplier"
        Me.chkSupplier.Size = New System.Drawing.Size(75, 17)
        Me.chkSupplier.TabIndex = 2
        Me.chkSupplier.Text = "Supplier"
        Me.chkSupplier.UseVisualStyleBackColor = True
        '
        'chkCustomer
        '
        Me.chkCustomer.Location = New System.Drawing.Point(208, 95)
        Me.chkCustomer.Name = "chkCustomer"
        Me.chkCustomer.Size = New System.Drawing.Size(75, 17)
        Me.chkCustomer.TabIndex = 3
        Me.chkCustomer.Text = "Customer"
        Me.chkCustomer.UseVisualStyleBackColor = True
        '
        'chkKaryawan
        '
        Me.chkKaryawan.Location = New System.Drawing.Point(293, 95)
        Me.chkKaryawan.Name = "chkKaryawan"
        Me.chkKaryawan.Size = New System.Drawing.Size(75, 17)
        Me.chkKaryawan.TabIndex = 4
        Me.chkKaryawan.Text = "Karyawan"
        Me.chkKaryawan.UseVisualStyleBackColor = True
        '
        'chkAktif
        '
        Me.chkAktif.AutoSize = True
        Me.chkAktif.Location = New System.Drawing.Point(321, 45)
        Me.chkAktif.Name = "chkAktif"
        Me.chkAktif.Size = New System.Drawing.Size(47, 17)
        Me.chkAktif.TabIndex = 43
        Me.chkAktif.Text = "Aktif"
        Me.chkAktif.UseVisualStyleBackColor = True
        '
        'frmMasterPersonal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(384, 389)
        Me.Controls.Add(Me.chkAktif)
        Me.Controls.Add(Me.chkKaryawan)
        Me.Controls.Add(Me.chkCustomer)
        Me.Controls.Add(Me.chkSupplier)
        Me.Controls.Add(Me.txtTelpCP)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.txtCP)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.gbJenisPersonal)
        Me.Controls.Add(Me.btnBaru)
        Me.Controls.Add(Me.btnHapus)
        Me.Controls.Add(Me.btnSimpan)
        Me.Controls.Add(Me.txtAlamat)
        Me.Controls.Add(Me.txtTelp)
        Me.Controls.Add(Me.btnSearchPersonal)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtNama)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtKode)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.Name = "frmMasterPersonal"
        Me.Text = "Master Personal / Instansi"
        Me.gbJenisPersonal.ResumeLayout(False)
        Me.gbJenisPersonal.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSearchPersonal As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtNama As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtKode As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtTelp As System.Windows.Forms.TextBox
    Friend WithEvents txtAlamat As System.Windows.Forms.TextBox
    Friend WithEvents btnBaru As System.Windows.Forms.Button
    Friend WithEvents btnHapus As System.Windows.Forms.Button
    Friend WithEvents btnSimpan As System.Windows.Forms.Button
    Friend WithEvents gbJenisPersonal As System.Windows.Forms.GroupBox
    Friend WithEvents rbInstansi As System.Windows.Forms.RadioButton
    Friend WithEvents rbPersonal As System.Windows.Forms.RadioButton
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtTelpCP As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtCP As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents chkSupplier As System.Windows.Forms.CheckBox
    Friend WithEvents chkCustomer As System.Windows.Forms.CheckBox
    Friend WithEvents chkKaryawan As System.Windows.Forms.CheckBox
    Friend WithEvents chkAktif As System.Windows.Forms.CheckBox
End Class
