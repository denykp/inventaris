﻿Public Class frmMasterAset

    Private Sub btnSearchAset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchAset.Click
        With frmCari
            .tabel = "ms_aset"
            .kolom = "*"
            .kondisi = ""
            .urut = ""

            .col_name = "kode_aset"

            If .ShowDialog = Windows.Forms.DialogResult.OK Then
                txtKodeAset.Text = .hasil
                load_dataAset()
            End If
        End With
    End Sub

    Public Sub load_dataAset()
        Try
            conn.Open()

            cmd = New SqlCommand("select * from ms_aset where kode_aset = '" & txtKodeAset.Text & "'", conn)
            reader = cmd.ExecuteReader
            If reader.Read Then
                txtNamaAset.Text = reader!nama_aset
                cmbMerk.Text = reader!merk
                cmbKategori.Text = reader!kategori
                txtCatatan.Text = reader!catatan
                If IsDBNull(reader!foto) = False Then
                    Dim datafoto As Byte() = reader!foto
                    Using stream As New IO.MemoryStream(datafoto)
                        pbAset.Image = New Bitmap(Image.FromStream(stream))
                    End Using
                    pbAset.SizeMode = PictureBoxSizeMode.Zoom
                End If
                chkAktif.Checked = reader!isAktif
            End If
            reader.Close()
            conn.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
            If conn.State Then conn.Close()
        End Try

    End Sub

    Private Sub btnSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSimpan.Click
        If txtNamaAset.Text = "" Then
            MsgBox("Nama aset tidak boleh kosong")
            Exit Sub
        End If

        Dim inTrans As Boolean
        Dim transaction As SqlTransaction = Nothing
        Try
            Dim query As String
            conn.Open()
            transaction = conn.BeginTransaction(IsolationLevel.ReadCommitted)
            inTrans = True
            'cmd = New SqlCommand("delete from ms_aset where kode_aset = '" & txtKodeAset.Text & "'", conn, transaction)
            'cmd.ExecuteNonQuery()
            If txtKodeAset.Text = "" Then
                txtKodeAset.Text = newIdUrut("ms_aset", "kode_aset", "ASET", 3)
                query = tambah_dataAset(True)
            Else
                query = tambah_dataAset(False)
            End If
            cmd = New SqlCommand(query, conn, transaction)
            cmd.ExecuteNonQuery()
            If Not pbAset.Image Is Nothing Then
                Dim stream As New IO.MemoryStream
                'isi stream dengan binary foto
                pbAset.Image.Save(stream, Imaging.ImageFormat.Jpeg)
                'command dengan parameter
                cmd = New SqlCommand("update ms_aset set foto = @foto where kode_aset = '" & txtKodeAset.Text & "'", conn, transaction)
                'isi parameter dengan data binary foto
                cmd.Parameters.Add("@foto", SqlDbType.VarBinary).Value = stream.GetBuffer
                cmd.ExecuteNonQuery()
            End If

            transaction.Commit()
            inTrans = False
            If conn.State Then conn.Close()
            MsgBox("Data telah tersimpan")
            reset_form()
        Catch ex As Exception
            If inTrans Then transaction.Rollback()
            If conn.State Then conn.Close()
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Function tambah_dataAset(ByVal isnew As Boolean)
        Dim nama_tabel As String
        Dim kolom() As String
        Dim data() As String

        ReDim kolom(6)
        ReDim data(6)

        nama_tabel = "ms_aset"
        kolom(0) = "kode_aset"
        kolom(1) = "nama_aset"
        kolom(2) = "merk"
        kolom(3) = "kategori"
        kolom(4) = "catatan"
        kolom(5) = "isAktif"

        data(0) = txtKodeAset.Text
        data(1) = txtNamaAset.Text
        data(2) = cmbMerk.Text
        data(3) = cmbKategori.Text
        data(4) = txtCatatan.Text
        data(5) = chkAktif.Checked

        If isnew Then
            Return tambah_data(nama_tabel, kolom, data)
        Else
            Return update_data(nama_tabel, kolom, data, "kode_aset = '" & txtKodeAset.Text & "'")
        End If
    End Function

    Private Sub frmMasterAset_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F5 Then btnSearchAset.PerformClick()
        If e.KeyCode = Keys.F2 Then btnSimpan.PerformClick()
        If e.KeyCode = Keys.Escape Then Me.Close()
    End Sub

    Private Sub frmMasterAset_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If blokirkarakter.Contains(e.KeyChar) = True Then
            hint.Show("Karakter yang anda ketik tidak diperbolehkan", Me.ActiveControl, 1500)
            e.Handled = True
        End If
    End Sub

    Private Sub frmMasterAset_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reset_form()
    End Sub

    Private Sub reset_form()
        txtKodeAset.Text = ""
        txtNamaAset.Text = ""
        cmbMerk.Text = ""
        cmbKategori.Text = ""
        txtCatatan.Text = ""
        txtDirectoryAset.Text = ""
        pbAset.Image = Nothing
        load_combo(cmbMerk, "ms_aset", "merk")
        load_combo(cmbKategori, "ms_aset", "kategori")
        chkAktif.Checked = False
    End Sub

    Private Sub btnBaru_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBaru.Click
        reset_form()
    End Sub

    Private Sub bntHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bntHapus.Click
        If MsgBox("Apakah anda yakin ingin menghapus data aset ini?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub
        Try
            conn.Open()
            cmd = New SqlCommand("delete from ms_aset where kode_aset = '" & txtKodeAset.Text & "'", conn)
            cmd.ExecuteNonQuery()
            conn.Close()
            MsgBox("Data aset berhasil dihapus")
            reset_form()
        Catch ex As Exception
            MsgBox(ex.Message)
            If conn.State Then conn.Close()
        End Try
    End Sub
End Class