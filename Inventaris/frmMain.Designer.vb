﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.MasterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MasterAsetToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MasterPersonalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TransaksiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PeminjamanAsetToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PengembalianAsetToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PersetujuanPeminjamanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PembelianAsetToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PenjualanAsetToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaporanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaporanPeminjamanAsetToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaporanPengembalianAsetToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaporanPembelianAsetToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaporanPenjualanAsetToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PengaturanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PengaturanUserToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GantiPasswordToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MasterToolStripMenuItem, Me.TransaksiToolStripMenuItem, Me.LaporanToolStripMenuItem, Me.PengaturanToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(678, 24)
        Me.MenuStrip1.TabIndex = 1
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'MasterToolStripMenuItem
        '
        Me.MasterToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MasterAsetToolStripMenuItem, Me.MasterPersonalToolStripMenuItem})
        Me.MasterToolStripMenuItem.Name = "MasterToolStripMenuItem"
        Me.MasterToolStripMenuItem.Size = New System.Drawing.Size(55, 20)
        Me.MasterToolStripMenuItem.Text = "Master"
        '
        'MasterAsetToolStripMenuItem
        '
        Me.MasterAsetToolStripMenuItem.Name = "MasterAsetToolStripMenuItem"
        Me.MasterAsetToolStripMenuItem.Size = New System.Drawing.Size(209, 22)
        Me.MasterAsetToolStripMenuItem.Text = "Master Aset"
        '
        'MasterPersonalToolStripMenuItem
        '
        Me.MasterPersonalToolStripMenuItem.Name = "MasterPersonalToolStripMenuItem"
        Me.MasterPersonalToolStripMenuItem.Size = New System.Drawing.Size(209, 22)
        Me.MasterPersonalToolStripMenuItem.Text = "Master Personal / Instansi"
        '
        'TransaksiToolStripMenuItem
        '
        Me.TransaksiToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PeminjamanAsetToolStripMenuItem, Me.PengembalianAsetToolStripMenuItem, Me.PersetujuanPeminjamanToolStripMenuItem, Me.PembelianAsetToolStripMenuItem, Me.PenjualanAsetToolStripMenuItem})
        Me.TransaksiToolStripMenuItem.Name = "TransaksiToolStripMenuItem"
        Me.TransaksiToolStripMenuItem.Size = New System.Drawing.Size(67, 20)
        Me.TransaksiToolStripMenuItem.Text = "Transaksi"
        '
        'PeminjamanAsetToolStripMenuItem
        '
        Me.PeminjamanAsetToolStripMenuItem.Name = "PeminjamanAsetToolStripMenuItem"
        Me.PeminjamanAsetToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.PeminjamanAsetToolStripMenuItem.Text = "Peminjaman Aset"
        '
        'PengembalianAsetToolStripMenuItem
        '
        Me.PengembalianAsetToolStripMenuItem.Name = "PengembalianAsetToolStripMenuItem"
        Me.PengembalianAsetToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.PengembalianAsetToolStripMenuItem.Text = "Pengembalian Aset"
        '
        'PersetujuanPeminjamanToolStripMenuItem
        '
        Me.PersetujuanPeminjamanToolStripMenuItem.Name = "PersetujuanPeminjamanToolStripMenuItem"
        Me.PersetujuanPeminjamanToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.PersetujuanPeminjamanToolStripMenuItem.Text = "Persetujuan Peminjaman"
        '
        'PembelianAsetToolStripMenuItem
        '
        Me.PembelianAsetToolStripMenuItem.Name = "PembelianAsetToolStripMenuItem"
        Me.PembelianAsetToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.PembelianAsetToolStripMenuItem.Text = "Pembelian Aset"
        '
        'PenjualanAsetToolStripMenuItem
        '
        Me.PenjualanAsetToolStripMenuItem.Name = "PenjualanAsetToolStripMenuItem"
        Me.PenjualanAsetToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.PenjualanAsetToolStripMenuItem.Text = "Penjualan Aset"
        '
        'LaporanToolStripMenuItem
        '
        Me.LaporanToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LaporanPeminjamanAsetToolStripMenuItem, Me.LaporanPengembalianAsetToolStripMenuItem, Me.LaporanPembelianAsetToolStripMenuItem, Me.LaporanPenjualanAsetToolStripMenuItem})
        Me.LaporanToolStripMenuItem.Name = "LaporanToolStripMenuItem"
        Me.LaporanToolStripMenuItem.Size = New System.Drawing.Size(62, 20)
        Me.LaporanToolStripMenuItem.Text = "Laporan"
        '
        'LaporanPeminjamanAsetToolStripMenuItem
        '
        Me.LaporanPeminjamanAsetToolStripMenuItem.Name = "LaporanPeminjamanAsetToolStripMenuItem"
        Me.LaporanPeminjamanAsetToolStripMenuItem.Size = New System.Drawing.Size(222, 22)
        Me.LaporanPeminjamanAsetToolStripMenuItem.Text = "Laporan Peminjaman Aset"
        '
        'LaporanPengembalianAsetToolStripMenuItem
        '
        Me.LaporanPengembalianAsetToolStripMenuItem.Name = "LaporanPengembalianAsetToolStripMenuItem"
        Me.LaporanPengembalianAsetToolStripMenuItem.Size = New System.Drawing.Size(222, 22)
        Me.LaporanPengembalianAsetToolStripMenuItem.Text = "Laporan Pengembalian Aset"
        '
        'LaporanPembelianAsetToolStripMenuItem
        '
        Me.LaporanPembelianAsetToolStripMenuItem.Name = "LaporanPembelianAsetToolStripMenuItem"
        Me.LaporanPembelianAsetToolStripMenuItem.Size = New System.Drawing.Size(222, 22)
        Me.LaporanPembelianAsetToolStripMenuItem.Text = "Laporan Pembelian Aset"
        '
        'LaporanPenjualanAsetToolStripMenuItem
        '
        Me.LaporanPenjualanAsetToolStripMenuItem.Name = "LaporanPenjualanAsetToolStripMenuItem"
        Me.LaporanPenjualanAsetToolStripMenuItem.Size = New System.Drawing.Size(222, 22)
        Me.LaporanPenjualanAsetToolStripMenuItem.Text = "Laporan Penjualan Aset"
        '
        'PengaturanToolStripMenuItem
        '
        Me.PengaturanToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PengaturanUserToolStripMenuItem, Me.GantiPasswordToolStripMenuItem})
        Me.PengaturanToolStripMenuItem.Name = "PengaturanToolStripMenuItem"
        Me.PengaturanToolStripMenuItem.Size = New System.Drawing.Size(80, 20)
        Me.PengaturanToolStripMenuItem.Text = "Pengaturan"
        Me.PengaturanToolStripMenuItem.Visible = False
        '
        'PengaturanUserToolStripMenuItem
        '
        Me.PengaturanUserToolStripMenuItem.Name = "PengaturanUserToolStripMenuItem"
        Me.PengaturanUserToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.PengaturanUserToolStripMenuItem.Text = "Pengaturan User"
        '
        'GantiPasswordToolStripMenuItem
        '
        Me.GantiPasswordToolStripMenuItem.Name = "GantiPasswordToolStripMenuItem"
        Me.GantiPasswordToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.GantiPasswordToolStripMenuItem.Text = "Ganti Password"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(678, 355)
        Me.Controls.Add(Me.MenuStrip1)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmMain"
        Me.Text = "Menu Utama"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents MasterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MasterAsetToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TransaksiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PeminjamanAsetToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PengembalianAsetToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PembelianAsetToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PenjualanAsetToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanPeminjamanAsetToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanPengembalianAsetToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanPembelianAsetToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanPenjualanAsetToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MasterPersonalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PengaturanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PengaturanUserToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GantiPasswordToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PersetujuanPeminjamanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
