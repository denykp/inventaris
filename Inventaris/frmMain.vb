﻿Public Class frmMain

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim C As Control

        For Each C In Me.Controls
            If TypeOf C Is MdiClient Then
                C.BackColor = Color.SteelBlue
                Exit For
            End If
        Next

        C = Nothing

        conn.ConnectionString = strcon()
    End Sub

    Private Sub MasterPersonalToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MasterPersonalToolStripMenuItem.Click
        Dim ChildForm As New frmMasterPersonal
        open_menuMaster(ChildForm, sender.ToString)
    End Sub

    Private Sub MasterAsetToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MasterAsetToolStripMenuItem.Click
        Dim ChildForm As New frmMasterAset
        open_menuMaster(ChildForm, sender.ToString)
    End Sub

    Private Sub PeminjamanAsetToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PeminjamanAsetToolStripMenuItem.Click
        Dim ChildForm As New frmTransPeminjaman
        open_menuTransaksi(ChildForm, sender.ToString)
    End Sub

    Private Sub PengembalianAsetToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PengembalianAsetToolStripMenuItem.Click
        Dim ChildForm As New frmTransPengembalian
        open_menuTransaksi(ChildForm, sender.ToString)
    End Sub

    Private Sub PembelianAsetToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PembelianAsetToolStripMenuItem.Click
        Dim ChildForm As New frmTransPembelian
        open_menuTransaksi(ChildForm, sender.ToString)
    End Sub

    Private Sub PenjualanAsetToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PenjualanAsetToolStripMenuItem.Click
        Dim ChildForm As New frmTransPenjualan
        open_menuTransaksi(ChildForm, sender.ToString)
    End Sub

    Private Sub ApprovalPeminjamanToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PersetujuanPeminjamanToolStripMenuItem.Click
        Dim ChildForm As New frmCari
        With ChildForm
            .tabel = "vw_peminjaman pm left join ms_personal_instansi mpi on pm.kode_karyawan = mpi.kode_personal_instansi"
            .kolom = "nomer_peminjaman,tanggal,nama_personal_instansi,userid"
            .kondisi = "persetujuan = '' or persetujuan is null or persetujuan = 'Pertimbangkan'"
            .urut = ""
            .isSearch = False

            .distinct = True
        End With
        open_menuTransaksi(ChildForm, sender.ToString)
    End Sub

    Private Sub LaporanPeminjamanAsetToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LaporanPeminjamanAsetToolStripMenuItem.Click
        Dim ChildForm As New frmLaporan
        ChildForm.filename = "Laporan Peminjaman.rpt"
        open_menuLaporan(ChildForm, sender.ToString)
    End Sub

    Private Sub LaporanPengembalianAsetToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LaporanPengembalianAsetToolStripMenuItem.Click
        Dim ChildForm As New frmLaporan
        ChildForm.filename = "Laporan Pengembalian.rpt"
        open_menuLaporan(ChildForm, sender.ToString)
    End Sub

    Private Sub LaporanPembelianAsetToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LaporanPembelianAsetToolStripMenuItem.Click
        Dim ChildForm As New frmLaporan
        ChildForm.filename = "Laporan Pembelian.rpt"
        open_menuLaporan(ChildForm, sender.ToString)
    End Sub

    Private Sub LaporanPenjualanAsetToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LaporanPenjualanAsetToolStripMenuItem.Click
        Dim ChildForm As New frmLaporan
        ChildForm.filename = "Laporan Penjualan.rpt"
        open_menuLaporan(ChildForm, sender.ToString)
    End Sub
End Class
