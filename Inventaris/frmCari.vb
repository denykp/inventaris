﻿Public Class frmCari

    Public col_name As String
    Public col_name2 As String
    Public hasil As String
    Public hasil2 As String

    Public tabel As String
    Public kolom As String
    Public distinct As Boolean
    Public kondisi As String
    Public urut As String

    Public isSearch As Boolean = True

    Private Sub load_grid()
        Try
            Dim da As New SqlDataAdapter
            Dim ds As New DataSet()
            Dim dv As New DataView
            Dim dt As DataTable

            Dim query As String
            Dim jumlah_baris As String

            jumlah_baris = Strings.Left(cmbJumlahBaris.Text, Len(cmbJumlahBaris.Text) - 6)
            If jumlah_baris <> "Semua" Then
                query = "select top " & jumlah_baris & " " & kolom & " from " & tabel
            Else
                query = "select " & kolom & " from " & tabel
            End If

            If distinct Then query = query.Replace("select", "select distinct")

            If kondisi <> "" Then query += " where " & kondisi

            If txtKataPencarian.Text <> "" Then
                da.SelectCommand = New SqlCommand(query, conn)
                da.Fill(ds, tabel)
                dt = ds.Tables(tabel)
                If query.Contains("where") Then query += " and"
                For i As Integer = 0 To dt.Columns.Count - 1
                    query += "," & dt.Columns.Item(i).ColumnName
                Next
                query = query.Substring(1) & " like '%" & txtKataPencarian.Text & "%'"
            End If
            If urut <> "" Then query += " order by " & urut
            da.SelectCommand = New SqlCommand(query, conn)

            da.Fill(ds, tabel)
            dv.Table = ds.Tables(0)
            dgvCari.DataSource = dv.Table
            For row As Integer = 0 To dv.Table.Columns.Count - 1
                If dv.Table.Columns(row).DataType.Name = "Decimal" Then
                    dgvCari.Columns(row).DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
                    dgvCari.Columns(row).DefaultCellStyle.Format = "#,##0.##"
                ElseIf dv.Table.Columns(row).DataType.Name = "DateTime" Then
                    dgvCari.Columns(row).DefaultCellStyle.Format = "dd/MM/yyyy"
                End If
            Next
        Catch ex As Exception
            MsgBox(ex.Message)
            'Me.Close()
        End Try
    End Sub

    Private Sub frmCari_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Me.WindowState = FormWindowState.Maximized
    End Sub

    Private Sub frmCari_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        col_name = ""
        col_name2 = ""

        tabel = ""
        kolom = ""
        kondisi = ""
        urut = ""
    End Sub

    Private Sub frmCari_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then Me.Close()
    End Sub

    Private Sub frmCari_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cmbJumlahBaris.SelectedIndex = 0
        load_grid()
    End Sub

    Private Sub data_dipilih()
        If dgvCari.RowCount > 0 And col_name <> "" Then
            hasil = dgvCari.Item(col_name, dgvCari.CurrentRow.Index).Value
            If col_name2 <> "" Then hasil2 = dgvCari.Item(col_name2, dgvCari.CurrentRow.Index).Value
            Me.DialogResult = Windows.Forms.DialogResult.OK
        End If
        Me.Close()
    End Sub

    Private Sub dgvCari_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvCari.CellDoubleClick
        If isSearch Then
            data_dipilih()
        Else
            load_detailPersetujuan(dgvCari.Item("nomer_peminjaman", e.RowIndex).Value)
        End If
    End Sub

    Private Sub dgvCari_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvCari.KeyDown
        If e.KeyCode = Keys.Enter Then data_dipilih()
    End Sub

    Private Sub btnCari_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCari.Click
        load_grid()
    End Sub

    Private Sub load_detailPersetujuan(ByVal nomer_peminjaman As String)
        frmPersetujuanDetail.lblNoTrans.Text = nomer_peminjaman
        open_dialog(frmPersetujuanDetail, "Persetujuan Detail")
    End Sub

    Private Sub cmbJumlahBaris_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbJumlahBaris.SelectedIndexChanged
        load_grid()
    End Sub

    Private Sub dgvCari_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvCari.CellContentClick

    End Sub
End Class