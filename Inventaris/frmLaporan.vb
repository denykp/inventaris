﻿Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine

Public Class frmLaporan

    Public filterTanggal As String
    Public filterKategori As String
    Public filterNoTrans As String
    Public filterAset As String
    Public formula As String
    Public filename As String

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        formula = ""
        If gbTanggal.Visible = True And filterTanggal <> "" And chkTanggal.Checked Then
            If dtSampai.Visible = True Then
                formula = formula & filterTanggal & ">=#" & dtDari.Value.ToString("yyyy/MM/dd") & "# and " & filterTanggal & "<=#" & dtSampai.Value.ToString("yyyy/MM/dd") & "#"
            Else
                formula = formula & filterTanggal & "=#" & dtSampai.Value.ToString("yyyy/MM/dd") & "#"
            End If
        End If
        If gbAset.Visible = True And filterAset <> "" And txtKodeAset.Text <> "" Then
            formula = formula & " and " & filterAset & "='" & txtKodeAset.Text & "'"
        End If
        If gbNoTrans.Visible = True And filterNoTrans <> "" And txtNoTrans.Text <> "" Then
            formula = filterNoTrans & "='" & txtNoTrans.Text & "'"
        End If
        With frmReportViewer
            .formula = formula
            .filename = filename
            .param(0) = dtDari.Value.ToString("dd-MMM-yyyy")
            .param(1) = dtSampai.Value.ToString("dd-MMM-yyyy")
            .param(2) = userId
            .ShowDialog()
        End With
    End Sub

    Private Sub frmLaporan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Select Case filename
            Case "Laporan Pembelian.rpt"
                gbNoTrans.Visible = True

                filterTanggal = "{t_pembelianH.tanggal}"
                filterNoTrans = "{t_pembelianH.nomer_pembelian}"
            Case "Laporan Penjualan.rpt"
                gbNoTrans.Visible = True

                filterTanggal = "{t_penjualanh.tanggal}"
                filterNoTrans = "{t_penjualanH.nomer_penjualan}"
            Case "Laporan Peminjaman.rpt"
                gbNoTrans.Visible = True

                filterTanggal = "{t_peminjamanH.tanggal}"
                filterNoTrans = "{t_peminjamanH.nomer_peminjaman}"
            Case "Laporan Pengembalian.rpt"
                gbNoTrans.Visible = True

                filterTanggal = "{t_pengembalianH.tanggal}"
                filterNoTrans = "{t_pengembalianH.nomer_pengembalian}"
        End Select
    End Sub

    Private Sub btnSearchNoTransaksi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchNoTransaksi.Click
        With frmCari
            Select Case filename
                Case "Laporan Pembelian.rpt"
                    .tabel = "t_pembelianH"
                    .kolom = "*"
                    .kondisi = ""
                    .urut = "nomer_pembelian desc"

                    .col_name = "nomer_pembelian"
                Case "Laporan Penjualan.rpt"
                    .tabel = "t_penjualanH"
                    .kolom = "*"
                    .kondisi = ""
                    .urut = "nomer_penjualan desc"

                    .col_name = "nomer_penjualan"
                Case "Laporan Peminjaman.rpt"
                    .tabel = "t_peminjamanH"
                    .kolom = "*"
                    .kondisi = ""
                    .urut = "nomer_peminjaman desc"

                    .col_name = "nomer_peminjaman"
                Case "Laporan Pengembalian.rpt"
                    .tabel = "vw_pengembalian pn left join ms_personal_instansi mpi on pn.kode_karyawan = mpi.kode_personal_instansi"
                    .kolom = "nomer_pengembalian,tanggal,nama_personal_instansi,catatan,userid"
                    .kondisi = ""
                    .urut = "nomer_pengembalian desc"
                    .distinct = True

                    .col_name = "nomer_pengembalian"
            End Select

            If .ShowDialog = Windows.Forms.DialogResult.OK Then
                txtNoTrans.Text = .hasil
            End If
        End With
    End Sub

    Private Sub btnOk_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.MouseHover
        btnOk.Font = New Font(btnOk.Text, 10, FontStyle.Underline)
    End Sub

    Private Sub btnOk_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.MouseLeave
        btnOk.Font = New Font(btnOk.Text, 8, FontStyle.Regular)
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnCancel_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseHover
        btnCancel.Font = New Font(btnCancel.Text, 10, FontStyle.Underline)
    End Sub

    Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
        btnCancel.Font = New Font(btnCancel.Text, 8, FontStyle.Regular)
    End Sub

    Private Sub btnSearchAset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchAset.Click
        With frmCari
            .tabel = "ms_aset"
            .kolom = "*"
            .kondisi = "isAktif = 'true'"
            .urut = ""

            .col_name = "kode_aset"
            .col_name2 = "nama_aset"

            If .ShowDialog = Windows.Forms.DialogResult.OK Then
                txtKodeAset.Text = .hasil
                lblNamaAset.Text = .hasil2
            End If
        End With
    End Sub
End Class