﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTransPembelian
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.gbHeader = New System.Windows.Forms.GroupBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtCatatan = New System.Windows.Forms.TextBox()
        Me.btnSearchSupplier = New System.Windows.Forms.Button()
        Me.lblNamaSupplier = New System.Windows.Forms.Label()
        Me.txtKodeSupplier = New System.Windows.Forms.TextBox()
        Me.dtpTransaksi = New System.Windows.Forms.DateTimePicker()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblNoTrans = New System.Windows.Forms.Label()
        Me.btnSearchTrans = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.rbRupiah = New System.Windows.Forms.RadioButton()
        Me.rbPersen = New System.Windows.Forms.RadioButton()
        Me.btnAsetBaru = New System.Windows.Forms.Button()
        Me.btnTambahkan = New System.Windows.Forms.Button()
        Me.txtDiskon = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.txtHarga = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.txtKuant = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.btnSearchAset = New System.Windows.Forms.Button()
        Me.lblNamaAset = New System.Windows.Forms.Label()
        Me.txtKodeAset = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.dgvDataPembelian = New System.Windows.Forms.DataGridView()
        Me.kode_aset = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nama_aset = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.kuant = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.harga = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tipe_diskon = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.diskon = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.netto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.jumlah = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnBaru = New System.Windows.Forms.Button()
        Me.bntHapus = New System.Windows.Forms.Button()
        Me.btnSimpan = New System.Windows.Forms.Button()
        Me.lblTotalItem = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.gbHeader.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvDataPembelian, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbHeader
        '
        Me.gbHeader.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbHeader.Controls.Add(Me.Label16)
        Me.gbHeader.Controls.Add(Me.Label17)
        Me.gbHeader.Controls.Add(Me.txtCatatan)
        Me.gbHeader.Controls.Add(Me.btnSearchSupplier)
        Me.gbHeader.Controls.Add(Me.lblNamaSupplier)
        Me.gbHeader.Controls.Add(Me.txtKodeSupplier)
        Me.gbHeader.Controls.Add(Me.dtpTransaksi)
        Me.gbHeader.Controls.Add(Me.Label9)
        Me.gbHeader.Controls.Add(Me.Label8)
        Me.gbHeader.Controls.Add(Me.Label7)
        Me.gbHeader.Controls.Add(Me.Label4)
        Me.gbHeader.Controls.Add(Me.Label3)
        Me.gbHeader.Controls.Add(Me.Label2)
        Me.gbHeader.Location = New System.Drawing.Point(12, 32)
        Me.gbHeader.Name = "gbHeader"
        Me.gbHeader.Size = New System.Drawing.Size(807, 95)
        Me.gbHeader.TabIndex = 53
        Me.gbHeader.TabStop = False
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(86, 68)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(10, 13)
        Me.Label16.TabIndex = 51
        Me.Label16.Text = ":"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(6, 68)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(76, 13)
        Me.Label17.TabIndex = 50
        Me.Label17.Text = "Nama Supplier"
        '
        'txtCatatan
        '
        Me.txtCatatan.Location = New System.Drawing.Point(438, 13)
        Me.txtCatatan.Multiline = True
        Me.txtCatatan.Name = "txtCatatan"
        Me.txtCatatan.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtCatatan.Size = New System.Drawing.Size(206, 47)
        Me.txtCatatan.TabIndex = 2
        '
        'btnSearchSupplier
        '
        Me.btnSearchSupplier.Location = New System.Drawing.Point(212, 37)
        Me.btnSearchSupplier.Name = "btnSearchSupplier"
        Me.btnSearchSupplier.Size = New System.Drawing.Size(75, 23)
        Me.btnSearchSupplier.TabIndex = 1
        Me.btnSearchSupplier.Text = "Search (F4)"
        Me.btnSearchSupplier.UseVisualStyleBackColor = True
        '
        'lblNamaSupplier
        '
        Me.lblNamaSupplier.Location = New System.Drawing.Point(103, 62)
        Me.lblNamaSupplier.Name = "lblNamaSupplier"
        Me.lblNamaSupplier.Size = New System.Drawing.Size(229, 27)
        Me.lblNamaSupplier.TabIndex = 41
        Me.lblNamaSupplier.Text = "-"
        Me.lblNamaSupplier.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtKodeSupplier
        '
        Me.txtKodeSupplier.Location = New System.Drawing.Point(106, 39)
        Me.txtKodeSupplier.Name = "txtKodeSupplier"
        Me.txtKodeSupplier.Size = New System.Drawing.Size(100, 20)
        Me.txtKodeSupplier.TabIndex = 1
        '
        'dtpTransaksi
        '
        Me.dtpTransaksi.CustomFormat = "dd MMM yyyy"
        Me.dtpTransaksi.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpTransaksi.Location = New System.Drawing.Point(106, 13)
        Me.dtpTransaksi.Name = "dtpTransaksi"
        Me.dtpTransaksi.Size = New System.Drawing.Size(100, 20)
        Me.dtpTransaksi.TabIndex = 0
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(418, 16)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(10, 13)
        Me.Label9.TabIndex = 40
        Me.Label9.Text = ":"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(86, 42)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(10, 13)
        Me.Label8.TabIndex = 39
        Me.Label8.Text = ":"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(86, 16)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(10, 13)
        Me.Label7.TabIndex = 38
        Me.Label7.Text = ":"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(337, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(44, 13)
        Me.Label4.TabIndex = 35
        Me.Label4.Text = "Catatan"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 42)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(73, 13)
        Me.Label3.TabIndex = 33
        Me.Label3.Text = "Kode Supplier"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 30
        Me.Label2.Text = "Tanggal"
        '
        'lblNoTrans
        '
        Me.lblNoTrans.AutoSize = True
        Me.lblNoTrans.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoTrans.Location = New System.Drawing.Point(171, 9)
        Me.lblNoTrans.Name = "lblNoTrans"
        Me.lblNoTrans.Size = New System.Drawing.Size(15, 20)
        Me.lblNoTrans.TabIndex = 52
        Me.lblNoTrans.Text = "-"
        '
        'btnSearchTrans
        '
        Me.btnSearchTrans.Location = New System.Drawing.Point(315, 9)
        Me.btnSearchTrans.Name = "btnSearchTrans"
        Me.btnSearchTrans.Size = New System.Drawing.Size(75, 23)
        Me.btnSearchTrans.TabIndex = 51
        Me.btnSearchTrans.Text = "Search (F5)"
        Me.btnSearchTrans.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(151, 9)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(14, 20)
        Me.Label6.TabIndex = 50
        Me.Label6.Text = ":"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(124, 20)
        Me.Label1.TabIndex = 49
        Me.Label1.Text = "No. Pembelian"
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.Panel2)
        Me.GroupBox1.Controls.Add(Me.btnAsetBaru)
        Me.GroupBox1.Controls.Add(Me.btnTambahkan)
        Me.GroupBox1.Controls.Add(Me.txtDiskon)
        Me.GroupBox1.Controls.Add(Me.Label30)
        Me.GroupBox1.Controls.Add(Me.Label31)
        Me.GroupBox1.Controls.Add(Me.txtHarga)
        Me.GroupBox1.Controls.Add(Me.Label26)
        Me.GroupBox1.Controls.Add(Me.Label27)
        Me.GroupBox1.Controls.Add(Me.txtKuant)
        Me.GroupBox1.Controls.Add(Me.Label23)
        Me.GroupBox1.Controls.Add(Me.Label24)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Controls.Add(Me.btnSearchAset)
        Me.GroupBox1.Controls.Add(Me.lblNamaAset)
        Me.GroupBox1.Controls.Add(Me.txtKodeAset)
        Me.GroupBox1.Controls.Add(Me.Label21)
        Me.GroupBox1.Controls.Add(Me.Label22)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 133)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(807, 97)
        Me.GroupBox1.TabIndex = 54
        Me.GroupBox1.TabStop = False
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.rbRupiah)
        Me.Panel2.Controls.Add(Me.rbPersen)
        Me.Panel2.Location = New System.Drawing.Point(431, 39)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(104, 21)
        Me.Panel2.TabIndex = 6
        '
        'rbRupiah
        '
        Me.rbRupiah.AutoSize = True
        Me.rbRupiah.Location = New System.Drawing.Point(63, 3)
        Me.rbRupiah.Name = "rbRupiah"
        Me.rbRupiah.Size = New System.Drawing.Size(39, 17)
        Me.rbRupiah.TabIndex = 8
        Me.rbRupiah.TabStop = True
        Me.rbRupiah.Text = "Rp"
        Me.rbRupiah.UseVisualStyleBackColor = True
        '
        'rbPersen
        '
        Me.rbPersen.AutoSize = True
        Me.rbPersen.Checked = True
        Me.rbPersen.Location = New System.Drawing.Point(4, 3)
        Me.rbPersen.Name = "rbPersen"
        Me.rbPersen.Size = New System.Drawing.Size(58, 17)
        Me.rbPersen.TabIndex = 7
        Me.rbPersen.TabStop = True
        Me.rbPersen.Text = "Persen"
        Me.rbPersen.UseVisualStyleBackColor = True
        '
        'btnAsetBaru
        '
        Me.btnAsetBaru.Location = New System.Drawing.Point(726, 63)
        Me.btnAsetBaru.Name = "btnAsetBaru"
        Me.btnAsetBaru.Size = New System.Drawing.Size(75, 23)
        Me.btnAsetBaru.TabIndex = 11
        Me.btnAsetBaru.Text = "Baru"
        Me.btnAsetBaru.UseVisualStyleBackColor = True
        '
        'btnTambahkan
        '
        Me.btnTambahkan.Location = New System.Drawing.Point(645, 63)
        Me.btnTambahkan.Name = "btnTambahkan"
        Me.btnTambahkan.Size = New System.Drawing.Size(75, 23)
        Me.btnTambahkan.TabIndex = 10
        Me.btnTambahkan.Text = "Tambahkan"
        Me.btnTambahkan.UseVisualStyleBackColor = True
        '
        'txtDiskon
        '
        Me.txtDiskon.Location = New System.Drawing.Point(431, 65)
        Me.txtDiskon.Name = "txtDiskon"
        Me.txtDiskon.Size = New System.Drawing.Size(100, 20)
        Me.txtDiskon.TabIndex = 9
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(411, 42)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(10, 13)
        Me.Label30.TabIndex = 85
        Me.Label30.Text = ":"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(332, 42)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(40, 13)
        Me.Label31.TabIndex = 84
        Me.Label31.Text = "Diskon"
        '
        'txtHarga
        '
        Me.txtHarga.Location = New System.Drawing.Point(431, 13)
        Me.txtHarga.Name = "txtHarga"
        Me.txtHarga.Size = New System.Drawing.Size(100, 20)
        Me.txtHarga.TabIndex = 5
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(411, 16)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(10, 13)
        Me.Label26.TabIndex = 81
        Me.Label26.Text = ":"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(332, 16)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(36, 13)
        Me.Label27.TabIndex = 80
        Me.Label27.Text = "Harga"
        '
        'txtKuant
        '
        Me.txtKuant.Location = New System.Drawing.Point(105, 65)
        Me.txtKuant.Name = "txtKuant"
        Me.txtKuant.Size = New System.Drawing.Size(100, 20)
        Me.txtKuant.TabIndex = 4
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(85, 68)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(10, 13)
        Me.Label23.TabIndex = 73
        Me.Label23.Text = ":"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(6, 68)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(51, 13)
        Me.Label24.TabIndex = 72
        Me.Label24.Text = "Kuantitas"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(85, 42)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(10, 13)
        Me.Label18.TabIndex = 71
        Me.Label18.Text = ":"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(6, 42)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(59, 13)
        Me.Label19.TabIndex = 70
        Me.Label19.Text = "Nama Aset"
        '
        'btnSearchAset
        '
        Me.btnSearchAset.Location = New System.Drawing.Point(211, 11)
        Me.btnSearchAset.Name = "btnSearchAset"
        Me.btnSearchAset.Size = New System.Drawing.Size(75, 23)
        Me.btnSearchAset.TabIndex = 3
        Me.btnSearchAset.Text = "Search (F3)"
        Me.btnSearchAset.UseVisualStyleBackColor = True
        '
        'lblNamaAset
        '
        Me.lblNamaAset.Location = New System.Drawing.Point(102, 36)
        Me.lblNamaAset.Name = "lblNamaAset"
        Me.lblNamaAset.Size = New System.Drawing.Size(229, 27)
        Me.lblNamaAset.TabIndex = 68
        Me.lblNamaAset.Text = "-"
        Me.lblNamaAset.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtKodeAset
        '
        Me.txtKodeAset.Location = New System.Drawing.Point(105, 13)
        Me.txtKodeAset.Name = "txtKodeAset"
        Me.txtKodeAset.Size = New System.Drawing.Size(100, 20)
        Me.txtKodeAset.TabIndex = 3
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(85, 16)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(10, 13)
        Me.Label21.TabIndex = 67
        Me.Label21.Text = ":"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(6, 16)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(56, 13)
        Me.Label22.TabIndex = 66
        Me.Label22.Text = "Kode Aset"
        '
        'dgvDataPembelian
        '
        Me.dgvDataPembelian.AllowUserToAddRows = False
        Me.dgvDataPembelian.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvDataPembelian.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDataPembelian.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.kode_aset, Me.nama_aset, Me.kuant, Me.harga, Me.tipe_diskon, Me.diskon, Me.netto, Me.jumlah})
        Me.dgvDataPembelian.Location = New System.Drawing.Point(12, 236)
        Me.dgvDataPembelian.Name = "dgvDataPembelian"
        Me.dgvDataPembelian.RowHeadersWidth = 30
        Me.dgvDataPembelian.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvDataPembelian.Size = New System.Drawing.Size(807, 218)
        Me.dgvDataPembelian.TabIndex = 12
        '
        'kode_aset
        '
        Me.kode_aset.HeaderText = "Kode Aset"
        Me.kode_aset.Name = "kode_aset"
        '
        'nama_aset
        '
        Me.nama_aset.HeaderText = "Nama Aset"
        Me.nama_aset.Name = "nama_aset"
        Me.nama_aset.Width = 150
        '
        'kuant
        '
        DataGridViewCellStyle1.Format = "#,##0"
        Me.kuant.DefaultCellStyle = DataGridViewCellStyle1
        Me.kuant.HeaderText = "Kuant"
        Me.kuant.Name = "kuant"
        Me.kuant.Width = 50
        '
        'harga
        '
        DataGridViewCellStyle2.Format = "#,##0"
        Me.harga.DefaultCellStyle = DataGridViewCellStyle2
        Me.harga.HeaderText = "Harga"
        Me.harga.Name = "harga"
        '
        'tipe_diskon
        '
        Me.tipe_diskon.HeaderText = "Tipe Diskon"
        Me.tipe_diskon.Name = "tipe_diskon"
        Me.tipe_diskon.Width = 50
        '
        'diskon
        '
        DataGridViewCellStyle3.Format = "#,##0"
        Me.diskon.DefaultCellStyle = DataGridViewCellStyle3
        Me.diskon.HeaderText = "Diskon"
        Me.diskon.Name = "diskon"
        Me.diskon.Width = 50
        '
        'netto
        '
        DataGridViewCellStyle4.Format = "#,##0"
        Me.netto.DefaultCellStyle = DataGridViewCellStyle4
        Me.netto.HeaderText = "Harga Bersih"
        Me.netto.Name = "netto"
        '
        'jumlah
        '
        DataGridViewCellStyle5.Format = "#,##0"
        Me.jumlah.DefaultCellStyle = DataGridViewCellStyle5
        Me.jumlah.HeaderText = "Jumlah"
        Me.jumlah.Name = "jumlah"
        '
        'btnBaru
        '
        Me.btnBaru.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnBaru.Location = New System.Drawing.Point(297, 535)
        Me.btnBaru.Name = "btnBaru"
        Me.btnBaru.Size = New System.Drawing.Size(75, 23)
        Me.btnBaru.TabIndex = 13
        Me.btnBaru.Text = "Baru"
        Me.btnBaru.UseVisualStyleBackColor = True
        '
        'bntHapus
        '
        Me.bntHapus.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.bntHapus.Location = New System.Drawing.Point(459, 535)
        Me.bntHapus.Name = "bntHapus"
        Me.bntHapus.Size = New System.Drawing.Size(75, 23)
        Me.bntHapus.TabIndex = 15
        Me.bntHapus.Text = "Hapus"
        Me.bntHapus.UseVisualStyleBackColor = True
        '
        'btnSimpan
        '
        Me.btnSimpan.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnSimpan.Location = New System.Drawing.Point(378, 535)
        Me.btnSimpan.Name = "btnSimpan"
        Me.btnSimpan.Size = New System.Drawing.Size(75, 23)
        Me.btnSimpan.TabIndex = 14
        Me.btnSimpan.Text = "Simpan (F2)"
        Me.btnSimpan.UseVisualStyleBackColor = True
        '
        'lblTotalItem
        '
        Me.lblTotalItem.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblTotalItem.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalItem.Location = New System.Drawing.Point(145, 457)
        Me.lblTotalItem.Name = "lblTotalItem"
        Me.lblTotalItem.Size = New System.Drawing.Size(129, 20)
        Me.lblTotalItem.TabIndex = 61
        Me.lblTotalItem.Text = "-"
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(125, 457)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(14, 20)
        Me.Label5.TabIndex = 60
        Me.Label5.Text = ":"
        '
        'Label20
        '
        Me.Label20.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(12, 457)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(90, 20)
        Me.Label20.TabIndex = 59
        Me.Label20.Text = "Total Item"
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal.Location = New System.Drawing.Point(690, 457)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(129, 20)
        Me.lblTotal.TabIndex = 82
        Me.lblTotal.Text = "0"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label42
        '
        Me.Label42.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.Location = New System.Drawing.Point(670, 457)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(14, 20)
        Me.Label42.TabIndex = 81
        Me.Label42.Text = ":"
        '
        'Label43
        '
        Me.Label43.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.Location = New System.Drawing.Point(540, 457)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(49, 20)
        Me.Label43.TabIndex = 80
        Me.Label43.Text = "Total"
        '
        'frmTransPembelian
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(831, 570)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.Label42)
        Me.Controls.Add(Me.Label43)
        Me.Controls.Add(Me.lblTotalItem)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.btnBaru)
        Me.Controls.Add(Me.bntHapus)
        Me.Controls.Add(Me.btnSimpan)
        Me.Controls.Add(Me.dgvDataPembelian)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.gbHeader)
        Me.Controls.Add(Me.lblNoTrans)
        Me.Controls.Add(Me.btnSearchTrans)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.Name = "frmTransPembelian"
        Me.Text = "Pembelian Aset"
        Me.gbHeader.ResumeLayout(False)
        Me.gbHeader.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.dgvDataPembelian, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbHeader As System.Windows.Forms.GroupBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtCatatan As System.Windows.Forms.TextBox
    Friend WithEvents btnSearchSupplier As System.Windows.Forms.Button
    Friend WithEvents lblNamaSupplier As System.Windows.Forms.Label
    Friend WithEvents txtKodeSupplier As System.Windows.Forms.TextBox
    Friend WithEvents dtpTransaksi As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblNoTrans As System.Windows.Forms.Label
    Friend WithEvents btnSearchTrans As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnAsetBaru As System.Windows.Forms.Button
    Friend WithEvents btnTambahkan As System.Windows.Forms.Button
    Friend WithEvents txtDiskon As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents txtHarga As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents txtKuant As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents btnSearchAset As System.Windows.Forms.Button
    Friend WithEvents lblNamaAset As System.Windows.Forms.Label
    Friend WithEvents txtKodeAset As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents dgvDataPembelian As System.Windows.Forms.DataGridView
    Friend WithEvents btnBaru As System.Windows.Forms.Button
    Friend WithEvents bntHapus As System.Windows.Forms.Button
    Friend WithEvents btnSimpan As System.Windows.Forms.Button
    Friend WithEvents lblTotalItem As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents rbRupiah As System.Windows.Forms.RadioButton
    Friend WithEvents rbPersen As System.Windows.Forms.RadioButton
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents kode_aset As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nama_aset As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents kuant As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents harga As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tipe_diskon As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents diskon As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents netto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents jumlah As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
