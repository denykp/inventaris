﻿Public Class frmTransPenjualan

    Private Sub btnTambahkan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTambahkan.Click
        dgvDataPenjualan.Rows.Add()
        Dim row As Integer = dgvDataPenjualan.RowCount - 1

        dgvDataPenjualan.Item("kode_aset", row).Value = txtKodeAset.Text
        dgvDataPenjualan.Item("nama_aset", row).Value = lblNamaAset.Text
        dgvDataPenjualan.Item("kuant", row).Value = txtKuant.Text
        dgvDataPenjualan.Item("harga", row).Value = txtHarga.Text

        hitung_total()
        reset_formDetail()
        txtKodeAset.Focus()
    End Sub

    Private Sub btnSearchTrans_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchTrans.Click
        With frmCari
            .tabel = "t_penjualanH"
            .kolom = "*"
            .kondisi = ""
            .urut = "nomer_penjualan desc"

            .col_name = "nomer_penjualan"

            If .ShowDialog = Windows.Forms.DialogResult.OK Then
                lblNoTrans.Text = .hasil
                load_dataPenjualan()
            End If
        End With
    End Sub

    Private Sub load_dataPenjualan()
        Try
            conn.Open()
            cmd = New SqlCommand("select pj.*,pi.nama_personal_instansi,a.nama_aset from vw_penjualan pj left join ms_personal_instansi pi on pi.kode_personal_instansi = pj.kode_customer " & _
                                 "left join ms_aset a on a.kode_aset = pj.kode_aset where nomer_penjualan = '" & lblNoTrans.Text & "'", conn)
            reader = cmd.ExecuteReader
            If reader.Read Then
                dtpTransaksi.Value = reader!tanggal
                txtKodeCustomer.Text = reader!kode_customer
                lblNamaCustomer.Text = reader!nama_personal_instansi
                txtCatatan.Text = reader!catatan

                'load data di grid
                With dgvDataPenjualan
                    .Rows.Clear()
                    Dim row As Integer
                    Do
                        .Rows.Add()
                        .Item("kode_aset", row).Value = reader!kode_aset
                        .Item("nama_aset", row).Value = reader!nama_aset
                        .Item("kuant", row).Value = reader!kuant
                        .Item("harga", row).Value = reader!harga

                        row += 1
                    Loop While reader.Read
                End With
            End If
            reader.Close()
            If conn.State Then conn.Close()

            hitung_total()
        Catch ex As Exception
            If conn.State Then conn.Close()
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub hitung_total()
        Try
            Dim totalItem As Integer
            Dim total As Decimal
            With dgvDataPenjualan
                For row As Integer = 0 To .RowCount - 1
                    .Item("jumlah", row).Value = .Item("kuant", row).Value * .Item("harga", row).Value
                    totalItem += .Item("kuant", row).Value
                    total += .Item("jumlah", row).Value
                Next
            End With

            lblTotalItem.Text = totalItem & " dari " & dgvDataPenjualan.RowCount & " baris"
            lblTotal.Text = Format(total, "#,##0")
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSimpan.Click
        Dim inTrans As Boolean
        Dim transaction As SqlTransaction = Nothing
        Try
            conn.Open()
            transaction = conn.BeginTransaction(IsolationLevel.ReadCommitted)
            inTrans = True

            If lblNoTrans.Text = "-" Then
                lblNoTrans.Text = newID("t_penjualanH", "nomer_penjualan", "PJ", "yyMM", 4, dtpTransaksi.Value)
            End If

            Dim queryDelete As String
            Dim queryInsert As String

            queryDelete = "delete from t_penjualanD where nomer_penjualan = '" & lblNoTrans.Text & "';"
            queryDelete += "delete from t_penjualanH where nomer_penjualan = '" & lblNoTrans.Text & "';"
            queryDelete += "delete from kartu_persediaan where nomer_transaksi = '" & lblNoTrans.Text & "';"
            cmd = New SqlCommand(queryDelete, conn, transaction)
            cmd.ExecuteNonQuery()

            queryInsert = add_headerPenjualan()
            With dgvDataPenjualan
                For row As Integer = 0 To dgvDataPenjualan.RowCount - 1
                    queryInsert += add_detailPenjualan(row)
                    queryInsert += insert_kartuPersediaan("Penjualan", lblNoTrans.Text, dtpTransaksi.Value, .Item("kode_aset", row).Value, .Item("kuant", row).Value * -1)
                Next
            End With

            cmd = New SqlCommand(queryInsert, conn, transaction)
            cmd.ExecuteNonQuery()

            transaction.Commit()
            inTrans = False
            If conn.State Then conn.Close()
            MsgBox("Data telah tersimpan")
            reset_form()
        Catch ex As Exception
            If inTrans Then transaction.Rollback()
            If conn.State Then conn.Close()
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Function add_headerPenjualan()
        Dim nama_tabel As String
        Dim kolom(5) As String
        Dim data(5) As String

        nama_tabel = "t_penjualanH"
        kolom(0) = "nomer_penjualan"
        kolom(1) = "tanggal"
        kolom(2) = "kode_customer"
        kolom(3) = "catatan"
        kolom(4) = "userid"

        data(0) = lblNoTrans.Text
        data(1) = dtpTransaksi.Value
        data(2) = txtKodeCustomer.Text
        data(3) = txtCatatan.Text
        data(4) = userId

        Return tambah_data(nama_tabel, kolom, data)
    End Function

    Private Function add_detailPenjualan(ByVal row As Integer)
        Dim nama_tabel As String
        Dim kolom(4) As String
        Dim data(4) As String

        nama_tabel = "t_penjualanD"
        kolom(0) = "nomer_penjualan"
        kolom(1) = "kode_aset"
        kolom(2) = "kuant"
        kolom(3) = "harga"

        data(0) = lblNoTrans.Text
        data(1) = dgvDataPenjualan.Item("kode_aset", row).Value
        data(2) = dgvDataPenjualan.Item("kuant", row).Value
        data(3) = dgvDataPenjualan.Item("harga", row).Value

        Return tambah_data(nama_tabel, kolom, data)
    End Function

    Private Sub reset_formDetail()
        txtKodeAset.Text = ""
        lblNamaAset.Text = "-"
        txtKuant.Text = 0
        txtHarga.Text = 0
    End Sub

    Private Sub reset_form()
        lblNoTrans.Text = "-"
        dtpTransaksi.Value = Now
        txtKodeCustomer.Text = ""
        lblNamaCustomer.Text = "-"
        txtCatatan.Text = ""
        dgvDataPenjualan.Rows.Clear()
        lblTotalItem.Text = "-"
        lblTotal.Text = 0

        reset_formDetail()
    End Sub

    Private Sub btnSearchCustomer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchCustomer.Click
        With frmCari
            .tabel = "ms_personal_instansi"
            .kolom = "*"
            .kondisi = "isCustomer = 'true'"
            .urut = ""

            .col_name = "kode_personal_instansi"
            .col_name2 = "nama_personal_instansi"

            If .ShowDialog = Windows.Forms.DialogResult.OK Then
                txtKodeCustomer.Text = .hasil
                lblNamaCustomer.Text = .hasil2
            End If
        End With
    End Sub

    Private Sub btnSearchAset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchAset.Click
        With frmCari
            .tabel = "ms_aset"
            .kolom = "*"
            .kondisi = "isAktif = 'true'"
            .urut = ""

            .col_name = "kode_aset"
            .col_name2 = "nama_aset"

            If .ShowDialog = Windows.Forms.DialogResult.OK Then
                txtKodeAset.Text = .hasil
                lblNamaAset.Text = .hasil2

                'load harga penjualan terakhir
                Try
                    conn.Open()

                    cmd = New SqlCommand("select top 1 harga from vw_penjualan where kode_aset = '" & txtKodeAset.Text & "' order by tanggal desc", conn)
                    reader = cmd.ExecuteReader
                    If reader.Read Then
                        txtHarga.Text = reader!harga
                    Else
                        txtHarga.Text = 0
                    End If
                    reader.Close()
                    conn.Close()
                Catch ex As Exception
                    If conn.State Then conn.Close()
                    MsgBox(ex.Message)
                End Try
            End If
        End With
    End Sub

    Private Sub btnAsetBaru_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAsetBaru.Click
        reset_formDetail()
    End Sub

    Private Sub frmTransPenjualan_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        reset_form()
    End Sub

    Private Sub dgvDataPenjualan_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvDataPenjualan.CellBeginEdit
        If e.ColumnIndex < 2 Or e.ColumnIndex > 5 Then e.Cancel = True
    End Sub

    Private Sub dgvDataPenjualan_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDataPenjualan.CellContentClick

    End Sub

    Private Sub dgvDataPenjualan_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDataPenjualan.CellEndEdit
        hitung_total()
    End Sub

    Private Sub btnBaru_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBaru.Click
        reset_form()
    End Sub

    Private Sub frmTransPenjualan_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F5 Then btnSearchTrans.PerformClick()
        If e.KeyCode = Keys.F4 Then btnSearchCustomer.PerformClick()
        If e.KeyCode = Keys.F3 Then btnSearchAset.PerformClick()
        If e.KeyCode = Keys.F2 Then btnSimpan.PerformClick()
        If e.KeyCode = Keys.Escape Then Me.Close()
    End Sub
End Class