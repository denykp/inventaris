﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPersetujuanDetail
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtCatatan = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.lblNamaKaryawan = New System.Windows.Forms.Label()
        Me.dtpTransaksi = New System.Windows.Forms.DateTimePicker()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lblNoTrans = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblTotalItem = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.dgvDataPersetujuan = New System.Windows.Forms.DataGridView()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.gbHeader = New System.Windows.Forms.GroupBox()
        Me.lblKodeKaryawan = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.kode_aset = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nama_aset = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.kuant = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.rencana_pengembalian = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.setuju = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.tolak = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.pertimbangkan = New System.Windows.Forms.DataGridViewButtonColumn()
        CType(Me.dgvDataPersetujuan, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbHeader.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(6, 68)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(85, 13)
        Me.Label17.TabIndex = 50
        Me.Label17.Text = "Nama Karyawan"
        '
        'txtCatatan
        '
        Me.txtCatatan.Location = New System.Drawing.Point(473, 13)
        Me.txtCatatan.Multiline = True
        Me.txtCatatan.Name = "txtCatatan"
        Me.txtCatatan.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtCatatan.Size = New System.Drawing.Size(206, 47)
        Me.txtCatatan.TabIndex = 37
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(86, 68)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(10, 13)
        Me.Label16.TabIndex = 51
        Me.Label16.Text = ":"
        '
        'lblNamaKaryawan
        '
        Me.lblNamaKaryawan.Location = New System.Drawing.Point(103, 62)
        Me.lblNamaKaryawan.Name = "lblNamaKaryawan"
        Me.lblNamaKaryawan.Size = New System.Drawing.Size(229, 27)
        Me.lblNamaKaryawan.TabIndex = 41
        Me.lblNamaKaryawan.Text = "-"
        Me.lblNamaKaryawan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpTransaksi
        '
        Me.dtpTransaksi.CustomFormat = "dd MMM yyyy"
        Me.dtpTransaksi.Enabled = False
        Me.dtpTransaksi.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpTransaksi.Location = New System.Drawing.Point(106, 13)
        Me.dtpTransaksi.Name = "dtpTransaksi"
        Me.dtpTransaksi.Size = New System.Drawing.Size(100, 20)
        Me.dtpTransaksi.TabIndex = 29
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(453, 16)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(10, 13)
        Me.Label9.TabIndex = 40
        Me.Label9.Text = ":"
        '
        'lblNoTrans
        '
        Me.lblNoTrans.AutoSize = True
        Me.lblNoTrans.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoTrans.Location = New System.Drawing.Point(171, 7)
        Me.lblNoTrans.Name = "lblNoTrans"
        Me.lblNoTrans.Size = New System.Drawing.Size(15, 20)
        Me.lblNoTrans.TabIndex = 103
        Me.lblNoTrans.Text = "-"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(151, 7)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(14, 20)
        Me.Label6.TabIndex = 101
        Me.Label6.Text = ":"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 30
        Me.Label2.Text = "Tanggal"
        '
        'lblTotalItem
        '
        Me.lblTotalItem.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblTotalItem.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalItem.Location = New System.Drawing.Point(141, 377)
        Me.lblTotalItem.Name = "lblTotalItem"
        Me.lblTotalItem.Size = New System.Drawing.Size(129, 20)
        Me.lblTotalItem.TabIndex = 112
        Me.lblTotalItem.Text = "-"
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(121, 377)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(14, 20)
        Me.Label5.TabIndex = 111
        Me.Label5.Text = ":"
        '
        'Label20
        '
        Me.Label20.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(8, 377)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(107, 20)
        Me.Label20.TabIndex = 110
        Me.Label20.Text = "Jumlah Item"
        '
        'dgvDataPersetujuan
        '
        Me.dgvDataPersetujuan.AllowUserToAddRows = False
        Me.dgvDataPersetujuan.AllowUserToDeleteRows = False
        Me.dgvDataPersetujuan.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvDataPersetujuan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDataPersetujuan.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.kode_aset, Me.nama_aset, Me.kuant, Me.rencana_pengembalian, Me.setuju, Me.tolak, Me.pertimbangkan})
        Me.dgvDataPersetujuan.Location = New System.Drawing.Point(12, 131)
        Me.dgvDataPersetujuan.Name = "dgvDataPersetujuan"
        Me.dgvDataPersetujuan.RowHeadersWidth = 30
        Me.dgvDataPersetujuan.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvDataPersetujuan.Size = New System.Drawing.Size(767, 243)
        Me.dgvDataPersetujuan.TabIndex = 106
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(86, 16)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(10, 13)
        Me.Label7.TabIndex = 38
        Me.Label7.Text = ":"
        '
        'gbHeader
        '
        Me.gbHeader.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbHeader.Controls.Add(Me.lblKodeKaryawan)
        Me.gbHeader.Controls.Add(Me.Label16)
        Me.gbHeader.Controls.Add(Me.Label17)
        Me.gbHeader.Controls.Add(Me.txtCatatan)
        Me.gbHeader.Controls.Add(Me.lblNamaKaryawan)
        Me.gbHeader.Controls.Add(Me.dtpTransaksi)
        Me.gbHeader.Controls.Add(Me.Label9)
        Me.gbHeader.Controls.Add(Me.Label8)
        Me.gbHeader.Controls.Add(Me.Label7)
        Me.gbHeader.Controls.Add(Me.Label4)
        Me.gbHeader.Controls.Add(Me.Label3)
        Me.gbHeader.Controls.Add(Me.Label2)
        Me.gbHeader.Location = New System.Drawing.Point(12, 30)
        Me.gbHeader.Name = "gbHeader"
        Me.gbHeader.Size = New System.Drawing.Size(767, 95)
        Me.gbHeader.TabIndex = 104
        Me.gbHeader.TabStop = False
        '
        'lblKodeKaryawan
        '
        Me.lblKodeKaryawan.Location = New System.Drawing.Point(102, 35)
        Me.lblKodeKaryawan.Name = "lblKodeKaryawan"
        Me.lblKodeKaryawan.Size = New System.Drawing.Size(229, 27)
        Me.lblKodeKaryawan.TabIndex = 52
        Me.lblKodeKaryawan.Text = "-"
        Me.lblKodeKaryawan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(86, 42)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(10, 13)
        Me.Label8.TabIndex = 39
        Me.Label8.Text = ":"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(336, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(44, 13)
        Me.Label4.TabIndex = 35
        Me.Label4.Text = "Catatan"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 42)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(82, 13)
        Me.Label3.TabIndex = 33
        Me.Label3.Text = "Kode Karyawan"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(138, 20)
        Me.Label1.TabIndex = 100
        Me.Label1.Text = "No. Peminjaman"
        '
        'kode_aset
        '
        Me.kode_aset.HeaderText = "Kode Aset"
        Me.kode_aset.Name = "kode_aset"
        '
        'nama_aset
        '
        Me.nama_aset.HeaderText = "Nama Aset"
        Me.nama_aset.Name = "nama_aset"
        Me.nama_aset.Width = 150
        '
        'kuant
        '
        Me.kuant.HeaderText = "Kuant"
        Me.kuant.Name = "kuant"
        Me.kuant.Width = 50
        '
        'rencana_pengembalian
        '
        Me.rencana_pengembalian.HeaderText = "Rencana Pengembalian"
        Me.rencana_pengembalian.Name = "rencana_pengembalian"
        '
        'setuju
        '
        Me.setuju.HeaderText = ""
        Me.setuju.Name = "setuju"
        Me.setuju.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.setuju.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.setuju.Text = "Setuju"
        Me.setuju.UseColumnTextForButtonValue = True
        '
        'tolak
        '
        Me.tolak.HeaderText = ""
        Me.tolak.Name = "tolak"
        Me.tolak.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.tolak.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.tolak.Text = "Tolak"
        Me.tolak.UseColumnTextForButtonValue = True
        '
        'pertimbangkan
        '
        Me.pertimbangkan.HeaderText = ""
        Me.pertimbangkan.Name = "pertimbangkan"
        Me.pertimbangkan.Text = "Pertimbangkan"
        Me.pertimbangkan.UseColumnTextForButtonValue = True
        '
        'frmPersetujuanDetail
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(790, 410)
        Me.Controls.Add(Me.lblNoTrans)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.lblTotalItem)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.dgvDataPersetujuan)
        Me.Controls.Add(Me.gbHeader)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmPersetujuanDetail"
        Me.Text = "frmPersetujuanDetail"
        CType(Me.dgvDataPersetujuan, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbHeader.ResumeLayout(False)
        Me.gbHeader.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtCatatan As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents lblNamaKaryawan As System.Windows.Forms.Label
    Friend WithEvents dtpTransaksi As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lblNoTrans As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblTotalItem As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents dgvDataPersetujuan As System.Windows.Forms.DataGridView
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents gbHeader As System.Windows.Forms.GroupBox
    Friend WithEvents lblKodeKaryawan As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents kode_aset As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nama_aset As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents kuant As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents rencana_pengembalian As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents setuju As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents tolak As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents pertimbangkan As System.Windows.Forms.DataGridViewButtonColumn
End Class
