﻿Public Class frmPersetujuanDetail

    Private Sub frmPersetujuanDetail_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

    End Sub

    Private Sub frmPersetujuanDetail_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        load_dataPeminjaman()
    End Sub

    Private Sub load_dataPeminjaman()
        Try
            conn.Open()
            cmd = New SqlCommand("select pm.*,pi.nama_personal_instansi,a.nama_aset from vw_peminjaman pm left join ms_personal_instansi pi on pi.kode_personal_instansi = pm.kode_karyawan " & _
                                 "left join ms_aset a on a.kode_aset = pm.kode_aset where nomer_peminjaman = '" & lblNoTrans.Text & "'", conn)
            reader = cmd.ExecuteReader
            If reader.Read Then
                dtpTransaksi.Value = reader!tanggal
                lblKodeKaryawan.Text = reader!kode_karyawan
                lblNamaKaryawan.Text = reader!nama_personal_instansi
                txtCatatan.Text = reader!catatan

                'load data di grid
                With dgvDataPersetujuan
                    .Rows.Clear()
                    Dim row As Integer
                    Do
                        .Rows.Add()
                        .Item("kode_aset", row).Value = reader!kode_aset
                        .Item("nama_aset", row).Value = reader!nama_aset
                        .Item("kuant", row).Value = reader!kuant
                        .Item("rencana_pengembalian", row).Value = reader!rencana_pengembalian
                        If Not IsDBNull(reader!persetujuan) Then
                            Select Case reader!persetujuan
                                Case "Setuju"
                                    .Rows(row).DefaultCellStyle.BackColor = Color.Green
                                Case "Tolak"
                                    .Rows(row).DefaultCellStyle.BackColor = Color.Red
                                Case "Pertimbangkan"
                                    .Rows(row).DefaultCellStyle.BackColor = Color.Yellow
                            End Select
                        End If

                        row += 1
                    Loop While reader.Read
                End With
            End If
            reader.Close()
            If conn.State Then conn.Close()

            hitung_total()
        Catch ex As Exception
            If conn.State Then conn.Close()
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub hitung_total()
        Try
            Dim totalItem As Integer
            With dgvDataPersetujuan
                For row As Integer = 0 To .RowCount - 1
                    totalItem += .Item("kuant", row).Value
                Next

                lblTotalItem.Text = totalItem & " dari " & .RowCount & " baris"
            End With
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub reset_form()
        lblNoTrans.Text = "-"
        dtpTransaksi.Value = Now
        lblKodeKaryawan.Text = ""
        lblNamaKaryawan.Text = "-"
        txtCatatan.Text = ""
        dgvDataPersetujuan.Rows.Clear()
        lblTotalItem.Text = "-"
    End Sub

    Private Sub dgvDataPersetujuan_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDataPersetujuan.CellContentClick
        With dgvDataPersetujuan
            Select Case e.ColumnIndex
                Case .Columns("setuju").Index
                    If MsgBox("Apakah anda yakin ingin menyetujui peminjaman ini?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub
                    If updatePersetujuan(.Item("kode_aset", e.RowIndex).Value, "Setuju") Then
                        MsgBox("Permintaan berhasil disetujui")
                        .Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.Green
                    End If
                Case .Columns("tolak").Index
                    If MsgBox("Apakah anda yakin ingin menolak peminjaman ini?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub
                    If updatePersetujuan(.Item("kode_aset", e.RowIndex).Value, "Tolak") Then
                        MsgBox("Permintaan berhasil ditolak")
                        .Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.Red
                    End If
                Case .Columns("pertimbangkan").Index
                    If MsgBox("Apakah anda yakin ingin mempertimbangkan peminjaman ini?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub
                    If updatePersetujuan(.Item("kode_aset", e.RowIndex).Value, "Pertimbangkan") Then
                        MsgBox("Permintaan sedang dipertimbangkan")
                        .Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.Yellow
                    End If
            End Select
        End With
    End Sub

    Private Function updatePersetujuan(ByVal kode_aset As String, ByVal persetujuan As String) As Boolean
        conn.Open()
        cmd = New SqlCommand("update t_peminjamand set persetujuan = '" & persetujuan & "', disetujui_oleh = '" & userId & "', disetujui_tanggal = '" & Now & "' where nomer_peminjaman = '" & lblNoTrans.Text & "' and kode_aset = '" & kode_aset & "'", conn)
        cmd.ExecuteNonQuery()
        conn.Close()
        Return True
    End Function
End Class