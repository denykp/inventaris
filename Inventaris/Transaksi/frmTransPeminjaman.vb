﻿Public Class frmTransPeminjaman

    Private Sub btnTambahkan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTambahkan.Click
        dgvDataPeminjaman.Rows.Add()
        Dim row As Integer = dgvDataPeminjaman.RowCount - 1

        dgvDataPeminjaman.Item("kode_aset", row).Value = txtKodeAset.Text
        dgvDataPeminjaman.Item("nama_aset", row).Value = lblNamaAset.Text
        dgvDataPeminjaman.Item("kuant", row).Value = txtKuant.Text

        hitung_total()
        reset_formDetail()
        txtKodeAset.Focus()
    End Sub

    Private Sub btnSearchTrans_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchTrans.Click
        With frmCari
            .tabel = "t_peminjamanH"
            .kolom = "*"
            .kondisi = ""
            .urut = "nomer_peminjaman desc"

            .col_name = "nomer_peminjaman"

            If .ShowDialog = Windows.Forms.DialogResult.OK Then
                lblNoTrans.Text = .hasil
                load_dataPeminjaman()
            End If
        End With
    End Sub

    Private Sub load_dataPeminjaman()
        On Error GoTo err
        conn.Open()
        cmd = New SqlCommand("select pm.*,pi.nama_personal_instansi,a.nama_aset from vw_peminjaman pm left join ms_personal_instansi pi on pi.kode_personal_instansi = pm.kode_karyawan " & _
                             "left join ms_aset a on a.kode_aset = pm.kode_aset where nomer_peminjaman = '" & lblNoTrans.Text & "'", conn)
        reader = cmd.ExecuteReader
        If reader.Read Then
            dtpTransaksi.Value = reader!tanggal
            dtpRencanaPengembalian.Value = reader!rencana_pengembalian
            txtKodeKaryawan.Text = reader!kode_karyawan
            lblNamaKaryawan.Text = reader!nama_personal_instansi
            txtCatatan.Text = reader!catatan

            'load data di grid
            With dgvDataPeminjaman
                .Rows.Clear()
                Dim row As Integer
                Do
                    .Rows.Add()
                    .Item("kode_aset", row).Value = reader!kode_aset
                    .Item("nama_aset", row).Value = reader!nama_aset
                    .Item("kuant", row).Value = reader!kuant

                    row += 1
                Loop While reader.Read
            End With
        End If
        reader.Close()
        If conn.State Then conn.Close()

        hitung_total()
        Exit Sub
err:
        MsgBox(Err.Description)
        Resume Next
    End Sub

    Private Sub hitung_total()
        Try
            Dim totalItem As Integer
            With dgvDataPeminjaman
                For row As Integer = 0 To .RowCount - 1
                    totalItem += .Item("kuant", row).Value
                Next
            End With

            lblTotalItem.Text = totalItem & " dari " & dgvDataPeminjaman.RowCount & " baris"
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSimpan.Click
        Dim inTrans As Boolean
        Dim transaction As SqlTransaction = Nothing
        Try
            conn.Open()
            transaction = conn.BeginTransaction(IsolationLevel.ReadCommitted)
            inTrans = True

            If lblNoTrans.Text = "-" Then
                lblNoTrans.Text = newID("t_peminjamanH", "nomer_peminjaman", "PM", "yyMM", 4, dtpTransaksi.Value)
            End If

            Dim queryDelete As String
            Dim queryInsert As String

            queryDelete = "delete from t_peminjamanD where nomer_peminjaman = '" & lblNoTrans.Text & "';"
            queryDelete += "delete from t_peminjamanH where nomer_peminjaman = '" & lblNoTrans.Text & "';"
            queryDelete += "delete from kartu_persediaan where nomer_transaksi = '" & lblNoTrans.Text & "';"
            cmd = New SqlCommand(queryDelete, conn, transaction)
            cmd.ExecuteNonQuery()

            queryInsert = add_headerPeminjaman()
            With dgvDataPeminjaman
                For row As Integer = 0 To dgvDataPeminjaman.RowCount - 1
                    queryInsert += add_detailPeminjaman(row)
                    queryInsert += insert_kartuPersediaan("Peminjaman", lblNoTrans.Text, dtpTransaksi.Value, .Item("kode_aset", row).Value, .Item("kuant", row).Value * -1)
                Next
            End With

            cmd = New SqlCommand(queryInsert, conn, transaction)
            cmd.ExecuteNonQuery()

            transaction.Commit()
            inTrans = False
            If conn.State Then conn.Close()
            MsgBox("Data telah tersimpan")
            reset_form()
        Catch ex As Exception
            If inTrans Then transaction.Rollback()
            If conn.State Then conn.Close()
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Function add_headerPeminjaman()
        Dim nama_tabel As String
        Dim kolom(6) As String
        Dim data(6) As String

        nama_tabel = "t_peminjamanH"
        kolom(0) = "nomer_peminjaman"
        kolom(1) = "tanggal"
        kolom(2) = "rencana_pengembalian"
        kolom(3) = "kode_karyawan"
        kolom(4) = "catatan"
        kolom(5) = "userid"

        data(0) = lblNoTrans.Text
        data(1) = dtpTransaksi.Value
        data(2) = dtpRencanaPengembalian.Value
        data(3) = txtKodeKaryawan.Text
        data(4) = txtCatatan.Text
        data(5) = userId

        Return tambah_data(nama_tabel, kolom, data)
    End Function

    Private Function add_detailPeminjaman(ByVal row As Integer)
        Dim nama_tabel As String
        Dim kolom(3) As String
        Dim data(3) As String

        nama_tabel = "t_peminjamanD"
        kolom(0) = "nomer_peminjaman"
        kolom(1) = "kode_aset"
        kolom(2) = "kuant"

        data(0) = lblNoTrans.Text
        data(1) = dgvDataPeminjaman.Item("kode_aset", row).Value
        data(2) = dgvDataPeminjaman.Item("kuant", row).Value

        Return tambah_data(nama_tabel, kolom, data)
    End Function

    Private Sub reset_formDetail()
        txtKodeAset.Text = ""
        lblNamaAset.Text = "-"
        txtKuant.Text = 0
    End Sub

    Private Sub reset_form()
        lblNoTrans.Text = "-"
        dtpTransaksi.Value = Now
        dtpRencanaPengembalian.Value = Now
        txtKodeKaryawan.Text = ""
        lblNamaKaryawan.Text = "-"
        txtCatatan.Text = ""
        dgvDataPeminjaman.Rows.Clear()
        lblTotalItem.Text = "-"

        reset_formDetail()
    End Sub

    Private Sub btnSearchKaryawan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchKaryawan.Click
        With frmCari
            .tabel = "ms_personal_instansi"
            .kolom = "*"
            .kondisi = "isKaryawan = 'true'"
            .urut = ""

            .col_name = "kode_personal_instansi"
            .col_name2 = "nama_personal_instansi"

            If .ShowDialog = Windows.Forms.DialogResult.OK Then
                txtKodeKaryawan.Text = .hasil
                lblNamaKaryawan.Text = .hasil2
            End If
        End With
    End Sub

    Private Sub btnSearchAset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchAset.Click
        With frmCari
            .tabel = "ms_aset"
            .kolom = "*"
            .kondisi = "isAktif = 'true'"
            .urut = ""

            .col_name = "kode_aset"
            .col_name2 = "nama_aset"

            If .ShowDialog = Windows.Forms.DialogResult.OK Then
                txtKodeAset.Text = .hasil
                lblNamaAset.Text = .hasil2
            End If
        End With
    End Sub

    Private Sub btnAsetBaru_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAsetBaru.Click
        reset_formDetail()
    End Sub

    Private Sub frmTransPeminjaman_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        reset_form()
    End Sub

    Private Sub dgvDataPeminjaman_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvDataPeminjaman.CellBeginEdit
        If e.ColumnIndex < 2 Then e.Cancel = True
    End Sub

    Private Sub dgvDataPeminjaman_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDataPeminjaman.CellContentClick

    End Sub

    Private Sub dgvDataPeminjaman_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDataPeminjaman.CellEndEdit
        hitung_total()
    End Sub

    Private Sub btnBaru_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBaru.Click
        reset_form()
    End Sub

    Private Sub frmTransPeminjaman_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F5 Then btnSearchTrans.PerformClick()
        If e.KeyCode = Keys.F4 Then btnSearchKaryawan.PerformClick()
        If e.KeyCode = Keys.F3 Then btnSearchAset.PerformClick()
        If e.KeyCode = Keys.F2 Then btnSimpan.PerformClick()
        If e.KeyCode = Keys.Escape Then Me.Close()
    End Sub

    Private Sub frmTransPeminjaman_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class