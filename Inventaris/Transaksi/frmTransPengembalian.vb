﻿Public Class frmTransPengembalian

    Private Sub frmTransPengembalian_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F5 Then btnSearchTrans.PerformClick()
        If e.KeyCode = Keys.F2 Then btnSimpan.PerformClick()
        If e.KeyCode = Keys.Escape Then Me.Close()
    End Sub

    Private Sub frmTransPengembalian_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnSearchPeminjaman_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchPeminjaman.Click
        With frmCari
            .tabel = "vw_peminjaman pm left join ms_personal_instansi mpi on pm.kode_karyawan = mpi.kode_personal_instansi"
            .kolom = "nomer_peminjaman,tanggal,nama_personal_instansi,catatan,userid"
            .kondisi = "persetujuan = 'Setuju' and kuant_kembali < kuant"
            .urut = ""
            .distinct = True

            .col_name = "nomer_peminjaman"

            If .ShowDialog = Windows.Forms.DialogResult.OK Then
                txtNoPeminjaman.Text = .hasil
                load_dataPeminjaman()
            End If
        End With
    End Sub

    Private Sub load_dataPeminjaman()
        On Error GoTo err
        conn.Open()
        cmd = New SqlCommand("select pm.*,pi.nama_personal_instansi,a.nama_aset from vw_peminjaman pm left join ms_personal_instansi pi on pi.kode_personal_instansi = pm.kode_karyawan " & _
                             "left join ms_aset a on a.kode_aset = pm.kode_aset where nomer_peminjaman = '" & txtNoPeminjaman.Text & "' and persetujuan='Setuju'", conn)
        reader = cmd.ExecuteReader
        If reader.Read Then
            dtpTanggalPeminjaman.Value = reader!tanggal
            dtpRencanaPengembalian.Value = reader!rencana_pengembalian
            lblNamaKaryawan.Text = reader!nama_personal_instansi
            txtCatatan.Text = reader!catatan

            'load data di grid
            With dgvDataPengembalian
                .Rows.Clear()
                Dim row As Integer
                Do
                    .Rows.Add()
                    .Item("kode_aset", row).Value = reader!kode_aset
                    .Item("nama_aset", row).Value = reader!nama_aset
                    .Item("kuant", row).Value = reader!kuant
                    .Item("sisa", row).Value = reader!kuant - reader!kuant_kembali
                    .Item("kembali", row).Value = 0

                    row += 1
                Loop While reader.Read
            End With
        End If
        reader.Close()
        If conn.State Then conn.Close()

        hitung_total()
        Exit Sub
err:
        MsgBox(Err.Description)
        Resume Next
    End Sub

    Private Sub hitung_total()
        Try
            Dim totalItem As Integer
            Dim validRow As Integer
            With dgvDataPengembalian
                For row As Integer = 0 To .RowCount - 1
                    totalItem += .Item("kembali", row).Value
                    If .Item("kembali", row).Value > 0 Then validRow += 1
                Next
            End With

            lblTotalKembali.Text = totalItem & " dari " & validRow & " baris"
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub reset_form()
        lblNoTrans.Text = "-"
        txtNoPeminjaman.Text = ""
        lblNamaKaryawan.Text = "-"
        dtpTanggalPeminjaman.Value = Now
        dtpRencanaPengembalian.Value = Now
        dtpTransaksi.Value = Now
        txtCatatan.Text = ""
        dgvDataPengembalian.Rows.Clear()
        lblTotalKembali.Text = "-"
    End Sub

    Private Sub dgvDataPengembalian_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs)
        If e.ColumnIndex <> dgvDataPengembalian.Columns("kembali").Index Then e.Cancel = True
    End Sub

    Private Sub dgvDataPengembalian_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)

    End Sub

    Private Sub dgvDataPengembalian_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
        hitung_total()
    End Sub

    Private Sub btnBaru_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBaru.Click
        reset_form()
    End Sub

    Private Sub btnSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSimpan.Click
        Dim inTrans As Boolean
        Dim transaction As SqlTransaction = Nothing
        Try
            conn.Open()
            transaction = conn.BeginTransaction(IsolationLevel.ReadCommitted)
            inTrans = True

            If lblNoTrans.Text = "-" Then
                lblNoTrans.Text = newID("t_pengembalianH", "nomer_pengembalian", "PN", "yyMM", 4, dtpTransaksi.Value)
            End If

            Dim queryDelete As String
            Dim queryInsert As String
            Dim queryUpdate As String = ""

            queryDelete = "delete from t_pengembalianD where nomer_pengembalian = '" & lblNoTrans.Text & "';"
            queryDelete += "delete from t_pengembalianH where nomer_pengembalian = '" & lblNoTrans.Text & "';"
            queryDelete += "delete from kartu_persediaan where nomer_transaksi = '" & lblNoTrans.Text & "';"
            cmd = New SqlCommand(queryDelete, conn, transaction)
            cmd.ExecuteNonQuery()

            queryInsert = add_headerPengembalian()
            With dgvDataPengembalian
                For row As Integer = 0 To dgvDataPengembalian.RowCount - 1
                    queryInsert += add_detailPengembalian(row)
                    queryInsert += insert_kartuPersediaan("Pengembalian", lblNoTrans.Text, dtpTransaksi.Value, .Item("kode_aset", row).Value, .Item("kembali", row).Value)
                Next
            End With

            cmd = New SqlCommand(queryInsert, conn, transaction)
            cmd.ExecuteNonQuery()

            'update qty kembali
            queryUpdate += "update t_peminjamanD set kuant_kembali = (select sum(kuant_kembali) from vw_pengembalian where nomer_peminjaman = t_peminjamanD.nomer_peminjaman and kode_aset = t_peminjamanD.kode_aset) where nomer_peminjaman = '" & txtNoPeminjaman.Text & "';"
            cmd = New SqlCommand(queryUpdate, conn, transaction)
            cmd.ExecuteNonQuery()

            transaction.Commit()
            inTrans = False
            If conn.State Then conn.Close()
            MsgBox("Data telah tersimpan")
            reset_form()
        Catch ex As Exception
            If inTrans Then transaction.Rollback()
            If conn.State Then conn.Close()
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Function add_headerPengembalian()
        Dim nama_tabel As String
        Dim kolom(5) As String
        Dim data(5) As String

        nama_tabel = "t_pengembalianH"
        kolom(0) = "nomer_pengembalian"
        kolom(1) = "nomer_peminjaman"
        kolom(2) = "tanggal"
        kolom(3) = "catatan"
        kolom(4) = "userid"

        data(0) = lblNoTrans.Text
        data(1) = txtNoPeminjaman.Text
        data(2) = dtpTransaksi.Value
        data(3) = txtCatatan.Text
        data(4) = userId

        Return tambah_data(nama_tabel, kolom, data)
    End Function

    Private Function add_detailPengembalian(ByVal row As Integer)
        Dim nama_tabel As String
        Dim kolom(4) As String
        Dim data(4) As String

        nama_tabel = "t_pengembalianD"
        kolom(0) = "nomer_pengembalian"
        kolom(1) = "kode_aset"
        kolom(2) = "sisa"
        kolom(3) = "kuant_kembali"

        data(0) = lblNoTrans.Text
        data(1) = dgvDataPengembalian.Item("kode_aset", row).Value
        data(2) = dgvDataPengembalian.Item("sisa", row).Value
        data(3) = dgvDataPengembalian.Item("kembali", row).Value

        Return tambah_data(nama_tabel, kolom, data)
    End Function


    Private Sub btnSearchTrans_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchTrans.Click
        With frmCari
            .tabel = "vw_pengembalian pn left join ms_personal_instansi mpi on pn.kode_karyawan = mpi.kode_personal_instansi"
            .kolom = "nomer_pengembalian,tanggal,nama_personal_instansi,catatan,userid"
            .kondisi = ""
            .urut = "nomer_pengembalian desc"
            .distinct = True

            .col_name = "nomer_pengembalian"

            If .ShowDialog = Windows.Forms.DialogResult.OK Then
                lblNoTrans.Text = .hasil
                load_dataPengembalian()
            End If
        End With
    End Sub

    Private Sub load_dataPengembalian()
        On Error GoTo err
        conn.Open()
        cmd = New SqlCommand("select distinct pn.*,pm.kuant,pi.nama_personal_instansi,a.nama_aset from vw_pengembalian pn " & _
                             "left join vw_peminjaman pm on pm.nomer_peminjaman = pn.nomer_peminjaman and pm.kode_aset = pn.kode_aset " & _
                             "left join ms_personal_instansi pi on pi.kode_personal_instansi = pn.kode_karyawan " & _
                             "left join ms_aset a on a.kode_aset = pn.kode_aset where nomer_pengembalian = '" & lblNoTrans.Text & "'", conn)
        reader = cmd.ExecuteReader
        If reader.Read Then
            txtNoPeminjaman.Text = reader!nomer_peminjaman
            dtpTransaksi.Value = reader!tanggal
            dtpRencanaPengembalian.Value = reader!rencana_pengembalian
            lblNamaKaryawan.Text = reader!nama_personal_instansi
            txtCatatan.Text = reader!catatan

            'load data di grid
            With dgvDataPengembalian
                .Rows.Clear()
                Dim row As Integer
                Do
                    .Rows.Add()
                    .Item("kode_aset", row).Value = reader!kode_aset
                    .Item("nama_aset", row).Value = reader!nama_aset
                    .Item("kuant", row).Value = reader!kuant
                    .Item("sisa", row).Value = reader!sisa
                    .Item("kembali", row).Value = reader!kuant_kembali
                    row += 1
                Loop While reader.Read
            End With
        End If
        reader.Close()
        If conn.State Then conn.Close()

        hitung_total()
        Exit Sub
err:
        MsgBox(Err.Description)
        Resume Next
    End Sub
End Class