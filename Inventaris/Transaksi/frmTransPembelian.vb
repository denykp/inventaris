﻿Public Class frmTransPembelian

    Private Sub btnTambahkan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTambahkan.Click
        dgvDataPembelian.Rows.Add()
        Dim row As Integer = dgvDataPembelian.RowCount - 1

        dgvDataPembelian.Item("kode_aset", row).Value = txtKodeAset.Text
        dgvDataPembelian.Item("nama_aset", row).Value = lblNamaAset.Text
        dgvDataPembelian.Item("kuant", row).Value = txtKuant.Text
        dgvDataPembelian.Item("harga", row).Value = txtHarga.Text
        dgvDataPembelian.Item("tipe_diskon", row).Value = IIf(rbPersen.Checked, "Persen", "Rupiah")
        dgvDataPembelian.Item("diskon", row).Value = txtDiskon.Text

        hitung_total()
        reset_formDetail()
        txtKodeAset.Focus()
    End Sub

    Private Sub btnSearchTrans_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchTrans.Click
        With frmCari
            .tabel = "t_pembelianH"
            .kolom = "*"
            .kondisi = ""
            .urut = "nomer_pembelian desc"

            .col_name = "nomer_pembelian"

            If .ShowDialog = Windows.Forms.DialogResult.OK Then
                lblNoTrans.Text = .hasil
                load_dataPembelian()
            End If
        End With
    End Sub

    Private Sub load_dataPembelian()
        Try
            conn.Open()
            cmd = New SqlCommand("select pb.*,pi.nama_personal_instansi,a.nama_aset from vw_pembelian pb left join ms_personal_instansi pi on pi.kode_personal_instansi = pb.kode_supplier " & _
                                 "left join ms_aset a on a.kode_aset = pb.kode_aset where nomer_pembelian = '" & lblNoTrans.Text & "'", conn)
            reader = cmd.ExecuteReader
            If reader.Read Then
                dtpTransaksi.Value = reader!tanggal
                txtKodeSupplier.Text = reader!kode_supplier
                lblNamaSupplier.Text = reader!nama_personal_instansi
                txtCatatan.Text = reader!catatan

                'load data di grid
                With dgvDataPembelian
                    .Rows.Clear()
                    Dim row As Integer
                    Do
                        .Rows.Add()
                        .Item("kode_aset", row).Value = reader!kode_aset
                        .Item("nama_aset", row).Value = reader!nama_aset
                        .Item("kuant", row).Value = reader!kuant
                        .Item("harga", row).Value = reader!harga
                        .Item("tipe_diskon", row).Value = reader!tipe_diskon
                        .Item("diskon", row).Value = reader!diskon
                        .Item("netto", row).Value = IIf(reader!tipe_diskon = "Persen", reader!harga * (100 - reader!diskon), reader!harga - reader!diskon)
                        .Item("jumlah", row).Value = reader!kuant * .Item("netto", row).Value

                        row += 1
                    Loop While reader.Read
                End With
            End If
            reader.Close()
            If conn.State Then conn.Close()

            hitung_total()
        Catch ex As Exception
            If conn.State Then conn.Close()
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub hitung_total()
        Try
            Dim totalItem As Integer
            Dim total As Decimal
            With dgvDataPembelian
                For row As Integer = 0 To .RowCount - 1
                    .Item("netto", row).Value = IIf(.Item("tipe_diskon", row).Value = "Persen", (.Item("harga", row).Value * (100 - .Item("diskon", row).Value)) / 100, .Item("harga", row).Value - .Item("diskon", row).Value)
                    .Item("jumlah", row).Value = .Item("kuant", row).Value * .Item("netto", row).Value
                    totalItem += .Item("kuant", row).Value
                    total += .Item("jumlah", row).Value
                Next
            End With

            lblTotalItem.Text = totalItem & " dari " & dgvDataPembelian.RowCount & " baris"
            lblTotal.Text = Format(total, "#,##0")
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSimpan.Click
        Dim inTrans As Boolean
        Dim transaction As SqlTransaction = Nothing
        Try
            conn.Open()
            transaction = conn.BeginTransaction(IsolationLevel.ReadCommitted)
            inTrans = True

            If lblNoTrans.Text = "-" Then
                lblNoTrans.Text = newID("t_pembelianH", "nomer_pembelian", "PB", "yyMM", 4, dtpTransaksi.Value)
            End If

            Dim queryDelete As String
            Dim queryInsert As String

            queryDelete = "delete from t_pembelianD where nomer_pembelian = '" & lblNoTrans.Text & "';"
            queryDelete += "delete from t_pembelianH where nomer_pembelian = '" & lblNoTrans.Text & "';"
            queryDelete += "delete from kartu_persediaan where nomer_transaksi = '" & lblNoTrans.Text & "';"
            cmd = New SqlCommand(queryDelete, conn, transaction)
            cmd.ExecuteNonQuery()

            queryInsert = add_headerPembelian()
            With dgvDataPembelian
                For row As Integer = 0 To .RowCount - 1
                    queryInsert += add_detailPembelian(row)
                    queryInsert += insert_kartuPersediaan("Pembelian", lblNoTrans.Text, dtpTransaksi.Value, .Item("kode_aset", row).Value, .Item("kuant", row).Value)
                Next
            End With

            cmd = New SqlCommand(queryInsert, conn, transaction)
            cmd.ExecuteNonQuery()

            'queryInsert = ""
            'cmd = New SqlCommand("select * from vw_pembelian where nomer_pembelian = '" & lblNoTrans.Text & "'", conn, transaction)
            'reader = cmd.ExecuteReader
            'While reader.Read
            '    queryInsert += insert_kartuPersediaan("Pembelian", reader!nomer_pembelian, reader!tanggal, reader!kode_aset, reader!kuant)
            'End While
            'reader.Close()

            'cmd = New SqlCommand(queryInsert, conn, transaction)
            'cmd.ExecuteNonQuery()
            transaction.Commit()
            inTrans = False
            If conn.State Then conn.Close()
            MsgBox("Data telah tersimpan")
            reset_form()
        Catch ex As Exception
            If inTrans Then transaction.Rollback()
            If conn.State Then conn.Close()
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Function add_headerPembelian()
        Dim nama_tabel As String
        Dim kolom(5) As String
        Dim data(5) As String

        nama_tabel = "t_pembelianH"
        kolom(0) = "nomer_pembelian"
        kolom(1) = "tanggal"
        kolom(2) = "kode_supplier"
        kolom(3) = "catatan"
        kolom(4) = "userid"

        data(0) = lblNoTrans.Text
        data(1) = dtpTransaksi.Value
        data(2) = txtKodeSupplier.Text
        data(3) = txtCatatan.Text
        data(4) = userId

        Return tambah_data(nama_tabel, kolom, data)
    End Function

    Private Function add_detailPembelian(ByVal row As Integer)
        Dim nama_tabel As String
        Dim kolom(6) As String
        Dim data(6) As String

        nama_tabel = "t_pembeliand"
        kolom(0) = "nomer_pembelian"
        kolom(1) = "kode_aset"
        kolom(2) = "kuant"
        kolom(3) = "harga"
        kolom(4) = "tipe_diskon"
        kolom(5) = "diskon"

        data(0) = lblNoTrans.Text
        data(1) = dgvDataPembelian.Item("kode_aset", row).Value
        data(2) = dgvDataPembelian.Item("kuant", row).Value
        data(3) = dgvDataPembelian.Item("harga", row).Value
        data(4) = dgvDataPembelian.Item("tipe_diskon", row).Value
        data(5) = dgvDataPembelian.Item("diskon", row).Value

        Return tambah_data(nama_tabel, kolom, data)
    End Function

    Private Sub reset_formDetail()
        txtKodeAset.Text = ""
        lblNamaAset.Text = "-"
        txtKuant.Text = 0
        txtHarga.Text = 0
        rbPersen.Checked = True
        txtDiskon.Text = 0
    End Sub

    Private Sub reset_form()
        lblNoTrans.Text = "-"
        dtpTransaksi.Value = Now
        txtKodeSupplier.Text = ""
        lblNamaSupplier.Text = "-"
        txtCatatan.Text = ""
        dgvDataPembelian.Rows.Clear()
        lblTotalItem.Text = "-"
        lblTotal.Text = 0

        reset_formDetail()
    End Sub

    Private Sub btnSearchSupplier_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchSupplier.Click
        With frmCari
            .tabel = "ms_personal_instansi"
            .kolom = "*"
            .kondisi = "isSupplier = 'true'"
            .urut = ""

            .col_name = "kode_personal_instansi"
            .col_name2 = "nama_personal_instansi"

            If .ShowDialog = Windows.Forms.DialogResult.OK Then
                txtKodeSupplier.Text = .hasil
                lblNamaSupplier.Text = .hasil2
            End If
        End With
    End Sub

    Private Sub btnSearchAset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchAset.Click
        With frmCari
            .tabel = "ms_aset"
            .kolom = "*"
            .kondisi = "isAktif = 'true'"
            .urut = ""

            .col_name = "kode_aset"
            .col_name2 = "nama_aset"

            If .ShowDialog = Windows.Forms.DialogResult.OK Then
                txtKodeAset.Text = .hasil
                lblNamaAset.Text = .hasil2

                'load harga pembelian terakhir
                Try
                    conn.Open()

                    cmd = New SqlCommand("select top 1 harga from vw_pembelian where kode_aset = '" & txtKodeAset.Text & "' order by tanggal desc", conn)
                    reader = cmd.ExecuteReader
                    If reader.Read Then
                        txtHarga.Text = reader!harga
                    Else
                        txtHarga.Text = 0
                    End If
                    reader.Close()
                    conn.Close()
                Catch ex As Exception
                    If conn.State Then conn.Close()
                    MsgBox(ex.Message)
                End Try
            End If
        End With
    End Sub

    Private Sub btnAsetBaru_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAsetBaru.Click
        reset_formDetail()
    End Sub

    Private Sub frmTransPembelian_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        reset_form()
    End Sub

    Private Sub dgvDataPembelian_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvDataPembelian.CellBeginEdit
        If e.ColumnIndex < 2 Or e.ColumnIndex > 5 Then e.Cancel = True
    End Sub

    Private Sub dgvDataPembelian_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDataPembelian.CellContentClick

    End Sub

    Private Sub dgvDataPembelian_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDataPembelian.CellEndEdit
        hitung_total()
    End Sub

    Private Sub btnBaru_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBaru.Click
        reset_form()
    End Sub

    Private Sub frmTransPembelian_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F5 Then btnSearchTrans.PerformClick()
        If e.KeyCode = Keys.F4 Then btnSearchSupplier.PerformClick()
        If e.KeyCode = Keys.F3 Then btnSearchAset.PerformClick()
        If e.KeyCode = Keys.F2 Then btnSimpan.PerformClick()
        If e.KeyCode = Keys.Escape Then Me.Close()
    End Sub

    Private Sub frmTransPembelian_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class