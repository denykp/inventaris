﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTransPengembalian
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.btnSearchTrans = New System.Windows.Forms.Button()
        Me.lblNoTrans = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.lblTotalKembali = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.bntHapus = New System.Windows.Forms.Button()
        Me.btnBaru = New System.Windows.Forms.Button()
        Me.btnSimpan = New System.Windows.Forms.Button()
        Me.gbHeader = New System.Windows.Forms.GroupBox()
        Me.dtpTransaksi = New System.Windows.Forms.DateTimePicker()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.dtpTanggalPeminjaman = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btnSearchPeminjaman = New System.Windows.Forms.Button()
        Me.txtNoPeminjaman = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtCatatan = New System.Windows.Forms.TextBox()
        Me.lblNamaKaryawan = New System.Windows.Forms.Label()
        Me.dtpRencanaPengembalian = New System.Windows.Forms.DateTimePicker()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.dgvDataPengembalian = New System.Windows.Forms.DataGridView()
        Me.kode_aset = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nama_aset = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.kuant = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.sisa = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.kembali = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gbHeader.SuspendLayout()
        CType(Me.dgvDataPengembalian, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnSearchTrans
        '
        Me.btnSearchTrans.Location = New System.Drawing.Point(336, 9)
        Me.btnSearchTrans.Name = "btnSearchTrans"
        Me.btnSearchTrans.Size = New System.Drawing.Size(75, 23)
        Me.btnSearchTrans.TabIndex = 102
        Me.btnSearchTrans.Text = "Search (F5)"
        Me.btnSearchTrans.UseVisualStyleBackColor = True
        '
        'lblNoTrans
        '
        Me.lblNoTrans.AutoSize = True
        Me.lblNoTrans.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoTrans.Location = New System.Drawing.Point(192, 9)
        Me.lblNoTrans.Name = "lblNoTrans"
        Me.lblNoTrans.Size = New System.Drawing.Size(15, 20)
        Me.lblNoTrans.TabIndex = 103
        Me.lblNoTrans.Text = "-"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(172, 9)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(14, 20)
        Me.Label6.TabIndex = 101
        Me.Label6.Text = ":"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(154, 20)
        Me.Label1.TabIndex = 100
        Me.Label1.Text = "No. Pengembalian"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(434, 72)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(44, 13)
        Me.Label4.TabIndex = 35
        Me.Label4.Text = "Catatan"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(432, 19)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(121, 13)
        Me.Label2.TabIndex = 30
        Me.Label2.Text = "Rencana Pengembalian"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(559, 16)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(10, 13)
        Me.Label7.TabIndex = 38
        Me.Label7.Text = ":"
        '
        'lblTotalKembali
        '
        Me.lblTotalKembali.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblTotalKembali.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalKembali.Location = New System.Drawing.Point(145, 429)
        Me.lblTotalKembali.Name = "lblTotalKembali"
        Me.lblTotalKembali.Size = New System.Drawing.Size(129, 20)
        Me.lblTotalKembali.TabIndex = 112
        Me.lblTotalKembali.Text = "-"
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(125, 429)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(14, 20)
        Me.Label5.TabIndex = 111
        Me.Label5.Text = ":"
        '
        'Label20
        '
        Me.Label20.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(12, 429)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(117, 20)
        Me.Label20.TabIndex = 110
        Me.Label20.Text = "Total Kembali"
        '
        'bntHapus
        '
        Me.bntHapus.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.bntHapus.Location = New System.Drawing.Point(484, 460)
        Me.bntHapus.Name = "bntHapus"
        Me.bntHapus.Size = New System.Drawing.Size(75, 23)
        Me.bntHapus.TabIndex = 108
        Me.bntHapus.Text = "Hapus"
        Me.bntHapus.UseVisualStyleBackColor = True
        '
        'btnBaru
        '
        Me.btnBaru.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnBaru.Location = New System.Drawing.Point(322, 460)
        Me.btnBaru.Name = "btnBaru"
        Me.btnBaru.Size = New System.Drawing.Size(75, 23)
        Me.btnBaru.TabIndex = 109
        Me.btnBaru.Text = "Baru"
        Me.btnBaru.UseVisualStyleBackColor = True
        '
        'btnSimpan
        '
        Me.btnSimpan.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnSimpan.Location = New System.Drawing.Point(403, 460)
        Me.btnSimpan.Name = "btnSimpan"
        Me.btnSimpan.Size = New System.Drawing.Size(75, 23)
        Me.btnSimpan.TabIndex = 107
        Me.btnSimpan.Text = "Simpan (F2)"
        Me.btnSimpan.UseVisualStyleBackColor = True
        '
        'gbHeader
        '
        Me.gbHeader.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbHeader.Controls.Add(Me.dtpTransaksi)
        Me.gbHeader.Controls.Add(Me.Label12)
        Me.gbHeader.Controls.Add(Me.Label13)
        Me.gbHeader.Controls.Add(Me.dtpTanggalPeminjaman)
        Me.gbHeader.Controls.Add(Me.Label3)
        Me.gbHeader.Controls.Add(Me.Label8)
        Me.gbHeader.Controls.Add(Me.btnSearchPeminjaman)
        Me.gbHeader.Controls.Add(Me.txtNoPeminjaman)
        Me.gbHeader.Controls.Add(Me.Label10)
        Me.gbHeader.Controls.Add(Me.Label11)
        Me.gbHeader.Controls.Add(Me.Label16)
        Me.gbHeader.Controls.Add(Me.Label17)
        Me.gbHeader.Controls.Add(Me.txtCatatan)
        Me.gbHeader.Controls.Add(Me.lblNamaKaryawan)
        Me.gbHeader.Controls.Add(Me.dtpRencanaPengembalian)
        Me.gbHeader.Controls.Add(Me.Label9)
        Me.gbHeader.Controls.Add(Me.Label7)
        Me.gbHeader.Controls.Add(Me.Label4)
        Me.gbHeader.Controls.Add(Me.Label2)
        Me.gbHeader.Location = New System.Drawing.Point(12, 32)
        Me.gbHeader.Name = "gbHeader"
        Me.gbHeader.Size = New System.Drawing.Size(857, 95)
        Me.gbHeader.TabIndex = 104
        Me.gbHeader.TabStop = False
        '
        'dtpTransaksi
        '
        Me.dtpTransaksi.CustomFormat = "dd MMM yyyy"
        Me.dtpTransaksi.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpTransaksi.Location = New System.Drawing.Point(579, 39)
        Me.dtpTransaksi.Name = "dtpTransaksi"
        Me.dtpTransaksi.Size = New System.Drawing.Size(100, 20)
        Me.dtpTransaksi.TabIndex = 59
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(559, 42)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(10, 13)
        Me.Label12.TabIndex = 61
        Me.Label12.Text = ":"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(432, 45)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(116, 13)
        Me.Label13.TabIndex = 60
        Me.Label13.Text = "Tanggal Pengembalian"
        '
        'dtpTanggalPeminjaman
        '
        Me.dtpTanggalPeminjaman.CustomFormat = "dd MMM yyyy"
        Me.dtpTanggalPeminjaman.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpTanggalPeminjaman.Location = New System.Drawing.Point(138, 66)
        Me.dtpTanggalPeminjaman.Name = "dtpTanggalPeminjaman"
        Me.dtpTanggalPeminjaman.Size = New System.Drawing.Size(100, 20)
        Me.dtpTanggalPeminjaman.TabIndex = 56
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(118, 69)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(10, 13)
        Me.Label3.TabIndex = 58
        Me.Label3.Text = ":"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(6, 69)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(106, 13)
        Me.Label8.TabIndex = 57
        Me.Label8.Text = "Tanggal Peminjaman"
        '
        'btnSearchPeminjaman
        '
        Me.btnSearchPeminjaman.Location = New System.Drawing.Point(244, 11)
        Me.btnSearchPeminjaman.Name = "btnSearchPeminjaman"
        Me.btnSearchPeminjaman.Size = New System.Drawing.Size(75, 23)
        Me.btnSearchPeminjaman.TabIndex = 55
        Me.btnSearchPeminjaman.Text = "Search"
        Me.btnSearchPeminjaman.UseVisualStyleBackColor = True
        '
        'txtNoPeminjaman
        '
        Me.txtNoPeminjaman.Location = New System.Drawing.Point(138, 13)
        Me.txtNoPeminjaman.Name = "txtNoPeminjaman"
        Me.txtNoPeminjaman.Size = New System.Drawing.Size(100, 20)
        Me.txtNoPeminjaman.TabIndex = 52
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(118, 16)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(10, 13)
        Me.Label10.TabIndex = 54
        Me.Label10.Text = ":"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(6, 16)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(84, 13)
        Me.Label11.TabIndex = 53
        Me.Label11.Text = "No. Peminjaman"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(118, 42)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(10, 13)
        Me.Label16.TabIndex = 51
        Me.Label16.Text = ":"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(6, 42)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(85, 13)
        Me.Label17.TabIndex = 50
        Me.Label17.Text = "Nama Karyawan"
        '
        'txtCatatan
        '
        Me.txtCatatan.Location = New System.Drawing.Point(579, 66)
        Me.txtCatatan.Name = "txtCatatan"
        Me.txtCatatan.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtCatatan.Size = New System.Drawing.Size(206, 20)
        Me.txtCatatan.TabIndex = 37
        '
        'lblNamaKaryawan
        '
        Me.lblNamaKaryawan.Location = New System.Drawing.Point(135, 36)
        Me.lblNamaKaryawan.Name = "lblNamaKaryawan"
        Me.lblNamaKaryawan.Size = New System.Drawing.Size(229, 27)
        Me.lblNamaKaryawan.TabIndex = 41
        Me.lblNamaKaryawan.Text = "-"
        Me.lblNamaKaryawan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpRencanaPengembalian
        '
        Me.dtpRencanaPengembalian.CustomFormat = "dd MMM yyyy"
        Me.dtpRencanaPengembalian.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpRencanaPengembalian.Location = New System.Drawing.Point(579, 13)
        Me.dtpRencanaPengembalian.Name = "dtpRencanaPengembalian"
        Me.dtpRencanaPengembalian.Size = New System.Drawing.Size(100, 20)
        Me.dtpRencanaPengembalian.TabIndex = 29
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(559, 69)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(10, 13)
        Me.Label9.TabIndex = 40
        Me.Label9.Text = ":"
        '
        'dgvDataPengembalian
        '
        Me.dgvDataPengembalian.AllowUserToAddRows = False
        Me.dgvDataPengembalian.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvDataPengembalian.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDataPengembalian.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.kode_aset, Me.nama_aset, Me.kuant, Me.sisa, Me.kembali})
        Me.dgvDataPengembalian.Location = New System.Drawing.Point(12, 133)
        Me.dgvDataPengembalian.Name = "dgvDataPengembalian"
        Me.dgvDataPengembalian.RowHeadersWidth = 30
        Me.dgvDataPengembalian.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvDataPengembalian.Size = New System.Drawing.Size(857, 293)
        Me.dgvDataPengembalian.TabIndex = 113
        '
        'kode_aset
        '
        Me.kode_aset.HeaderText = "Kode Aset"
        Me.kode_aset.Name = "kode_aset"
        '
        'nama_aset
        '
        Me.nama_aset.HeaderText = "Nama Aset"
        Me.nama_aset.Name = "nama_aset"
        Me.nama_aset.Width = 150
        '
        'kuant
        '
        DataGridViewCellStyle4.Format = "#,##0"
        Me.kuant.DefaultCellStyle = DataGridViewCellStyle4
        Me.kuant.HeaderText = "Kuant"
        Me.kuant.Name = "kuant"
        '
        'sisa
        '
        Me.sisa.HeaderText = "Sisa"
        Me.sisa.Name = "sisa"
        '
        'kembali
        '
        Me.kembali.HeaderText = "Kembali"
        Me.kembali.Name = "kembali"
        '
        'frmTransPengembalian
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(881, 495)
        Me.Controls.Add(Me.dgvDataPengembalian)
        Me.Controls.Add(Me.btnSearchTrans)
        Me.Controls.Add(Me.lblNoTrans)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblTotalKembali)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.bntHapus)
        Me.Controls.Add(Me.btnBaru)
        Me.Controls.Add(Me.btnSimpan)
        Me.Controls.Add(Me.gbHeader)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.Name = "frmTransPengembalian"
        Me.Text = "frmTransPengembalian"
        Me.gbHeader.ResumeLayout(False)
        Me.gbHeader.PerformLayout()
        CType(Me.dgvDataPengembalian, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSearchTrans As System.Windows.Forms.Button
    Friend WithEvents lblNoTrans As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lblTotalKembali As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents bntHapus As System.Windows.Forms.Button
    Friend WithEvents btnBaru As System.Windows.Forms.Button
    Friend WithEvents btnSimpan As System.Windows.Forms.Button
    Friend WithEvents gbHeader As System.Windows.Forms.GroupBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtCatatan As System.Windows.Forms.TextBox
    Friend WithEvents lblNamaKaryawan As System.Windows.Forms.Label
    Friend WithEvents dtpRencanaPengembalian As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents btnSearchPeminjaman As System.Windows.Forms.Button
    Friend WithEvents txtNoPeminjaman As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents dtpTanggalPeminjaman As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents dtpTransaksi As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents dgvDataPengembalian As System.Windows.Forms.DataGridView
    Friend WithEvents kode_aset As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nama_aset As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents kuant As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents sisa As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents kembali As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
