﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCari
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgvCari = New System.Windows.Forms.DataGridView()
        Me.txtKataPencarian = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnCari = New System.Windows.Forms.Button()
        Me.cmbKolomPencarian = New System.Windows.Forms.ComboBox()
        Me.cmbJumlahBaris = New System.Windows.Forms.ComboBox()
        CType(Me.dgvCari, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvCari
        '
        Me.dgvCari.AllowUserToAddRows = False
        Me.dgvCari.AllowUserToDeleteRows = False
        Me.dgvCari.AllowUserToResizeRows = False
        Me.dgvCari.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvCari.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCari.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgvCari.Location = New System.Drawing.Point(12, 33)
        Me.dgvCari.Name = "dgvCari"
        Me.dgvCari.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCari.Size = New System.Drawing.Size(751, 216)
        Me.dgvCari.TabIndex = 0
        '
        'txtKataPencarian
        '
        Me.txtKataPencarian.Location = New System.Drawing.Point(101, 6)
        Me.txtKataPencarian.Name = "txtKataPencarian"
        Me.txtKataPencarian.Size = New System.Drawing.Size(170, 20)
        Me.txtKataPencarian.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(86, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Kata Pencarian :"
        '
        'btnCari
        '
        Me.btnCari.Location = New System.Drawing.Point(277, 4)
        Me.btnCari.Name = "btnCari"
        Me.btnCari.Size = New System.Drawing.Size(51, 23)
        Me.btnCari.TabIndex = 3
        Me.btnCari.Text = "Cari"
        Me.btnCari.UseVisualStyleBackColor = True
        '
        'cmbKolomPencarian
        '
        Me.cmbKolomPencarian.FormattingEnabled = True
        Me.cmbKolomPencarian.Location = New System.Drawing.Point(334, 6)
        Me.cmbKolomPencarian.Name = "cmbKolomPencarian"
        Me.cmbKolomPencarian.Size = New System.Drawing.Size(121, 21)
        Me.cmbKolomPencarian.TabIndex = 5
        '
        'cmbJumlahBaris
        '
        Me.cmbJumlahBaris.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbJumlahBaris.FormattingEnabled = True
        Me.cmbJumlahBaris.Items.AddRange(New Object() {"100 Baris", "1000 Baris", "5000 Baris", "Semua Baris"})
        Me.cmbJumlahBaris.Location = New System.Drawing.Point(679, 6)
        Me.cmbJumlahBaris.Name = "cmbJumlahBaris"
        Me.cmbJumlahBaris.Size = New System.Drawing.Size(84, 21)
        Me.cmbJumlahBaris.TabIndex = 6
        '
        'frmCari
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(775, 261)
        Me.Controls.Add(Me.cmbJumlahBaris)
        Me.Controls.Add(Me.cmbKolomPencarian)
        Me.Controls.Add(Me.btnCari)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtKataPencarian)
        Me.Controls.Add(Me.dgvCari)
        Me.KeyPreview = True
        Me.Name = "frmCari"
        Me.Text = "Pencarian"
        CType(Me.dgvCari, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvCari As System.Windows.Forms.DataGridView
    Friend WithEvents txtKataPencarian As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnCari As System.Windows.Forms.Button
    Friend WithEvents cmbKolomPencarian As System.Windows.Forms.ComboBox
    Friend WithEvents cmbJumlahBaris As System.Windows.Forms.ComboBox
End Class
