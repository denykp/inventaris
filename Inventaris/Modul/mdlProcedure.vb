﻿Module mdlProcedure
    Public Function tambah_data(ByVal nama_tabel As String, ByVal kolom As String(), ByVal data As String())
        Dim query As String
        Dim i As Integer
        query = "insert into " & nama_tabel & " ("
        For i = 0 To kolom.Length - 2
            query = query & kolom(i) & ","
        Next
        query = Left(query, Len(query) - 1)
        query = query & ") values ('"
        For i = 0 To data.Length - 2
            query = query & data(i) & "','"
        Next
        query = Left(query, Len(query) - 2)
        query = query & ")"

        tambah_data = query
    End Function

    Public Function update_data(ByVal table_name As String, ByVal kolom() As String, ByVal data() As String, ByVal kondisi As String)
        Dim query As String
        query = "update " & table_name & " set "
        For i = 0 To UBound(kolom) - 1
            query = query & kolom(i) & "='" & data(i) & "',"
        Next
        query = Left(query, Len(query) - 1) & " where " & kondisi
        Return query
    End Function

    Public Sub load_combo(ByVal combo As ComboBox, ByVal tabel As String, ByVal kolom As String, Optional ByVal kondisi As String = "")
        Dim conn As New SqlConnection(strcon)
        conn.Open()
        cmd = New SqlCommand("select distinct " & kolom & " from " & tabel & " " & kondisi, conn)
        reader = cmd.ExecuteReader
        combo.Items.Clear()
        While reader.Read
            combo.Items.Add(reader(kolom))
        End While
        reader.Close()
        conn.Close()
    End Sub

    Public Sub load_combodet(ByVal combo As ComboBox, ByVal tabel As String, ByVal kolom As String, ByVal kondisi As String, ByVal conn As SqlConnection)
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        Dim dt As New DataTable

        cmd = New SqlCommand("select distinct " & kolom & " from " & tabel & " " & kondisi, conn)
        da.SelectCommand = cmd
        da.Fill(dt)
        Dim emptyrow As DataRow = dt.NewRow
        emptyrow(0) = ""
        dt.Rows.InsertAt(emptyrow, 0)
        combo.DataSource = dt
        combo.DisplayMember = Left(kolom, kolom.IndexOf(","))
        combo.ValueMember = Right(kolom, kolom.IndexOf(","))
    End Sub

    Public Function newID(ByVal table_name As String, ByVal key As String, ByVal prefix As String, ByVal dateformat As String, ByVal numlen As Integer, ByVal tanggal As Date)
        Dim conn As New SqlConnection(strcon)
        Dim cmd As SqlCommand
        Dim dr As SqlDataReader
        Dim numformat As String = ""
        numformat = numformat.PadLeft(numlen, "0")
        newID = ""
        conn.Open()
        Try
            Dim no As String = ""
            cmd = New SqlCommand("select top 1 " & key & " from " & table_name & " where left(" & key & "," & (Len(prefix & dateformat)) & ")='" & prefix & Val(Format(tanggal, dateformat)) & "' order by " & key & " desc", conn)
            dr = cmd.ExecuteReader()
            If dr.Read() Then
                no = prefix & tanggal.ToString(dateformat) & Format((CLng(Strings.Right(dr(0).ToString, numlen)) + 1), numformat)
            Else
                no = prefix & tanggal.ToString(dateformat) & Format(1, numformat)
            End If
            dr.Close()
            newID = no
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Function

    Public Function newIdUrut(ByVal table_name As String, ByVal key As String, ByVal prefix As String, ByVal numlen As Integer)
        Dim conn As New SqlConnection(strcon)
        Dim cmd As SqlCommand
        Dim dr As SqlDataReader
        Dim numformat As String = ""
        numformat = numformat.PadLeft(numlen, "0")
        conn.Open()
        newIdUrut = ""
        Try
            Dim no As String = ""
            cmd = New SqlCommand("select top 1 " & key & " from " & table_name & " where left(" & key & "," & (Len(prefix)) & ")='" & prefix & "' order by " & key & " desc", conn)
            dr = cmd.ExecuteReader()
            If dr.Read() Then
                no = prefix & "-" & Format((CLng(Strings.Right(dr(0).ToString, numlen)) + 1), numformat)
            Else
                no = prefix & "-" & Format(1, numformat)
            End If
            dr.Close()
            newIdUrut = no
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Function

    Public Function open_menuMaster(ByVal ChildForm As Form, ByVal title As String) As Boolean
        open_menuMaster = False
        For Each Form As Form In Application.OpenForms
            If TypeOf Form Is Form Then
                If Form.Name = ChildForm.Name Then
                    Form.Activate()
                    Return True
                End If
            End If
        Next

        ChildForm.Text = title

        ' Make it a child of this MDI form before showing it.
        ChildForm.MdiParent = frmMain
        ChildForm.BackColor = bgColorMaster
        ChildForm.Show()
        ChildForm.Location = New Point(0, 0)
    End Function

    Public Function open_menuTransaksi(ByVal ChildForm As Form, ByVal title As String) As Boolean
        open_menuTransaksi = False
        For Each Form As Form In Application.OpenForms
            If TypeOf Form Is Form Then
                If Form.Name = ChildForm.Name Then
                    Form.Activate()
                    Return True
                End If
            End If
        Next

        ChildForm.Text = title

        ' Make it a child of this MDI form before showing it.
        ChildForm.MdiParent = frmMain
        ChildForm.BackColor = bgColorMaster
        ChildForm.Show()
        ChildForm.Location = New Point(0, 0)
    End Function

    Public Function open_menuLaporan(ByVal ChildForm As Form, ByVal title As String) As Boolean
        open_menuLaporan = False
        For Each Form As Form In Application.OpenForms
            If TypeOf Form Is Form Then
                If Form.Name = ChildForm.Name Then
                    Form.Activate()
                    Return True
                End If
            End If
        Next

        ChildForm.Text = title

        ' Make it a child of this MDI form before showing it.
        ChildForm.MdiParent = frmMain
        ChildForm.BackColor = bgColorMaster
        ChildForm.Show()
        ChildForm.Location = New Point(0, 0)
    End Function

    Public Function open_dialog(ByVal DialogForm As Form, ByVal title As String) As Boolean
        open_dialog = False
        With DialogForm
            .Text = title

            .BackColor = bgColorMaster
            .StartPosition = FormStartPosition.CenterScreen
            .ShowDialog()
        End With
    End Function

    Public Function insert_kartuPersediaan(ByVal tipe As String, ByVal nomer_transaksi As String, ByVal tanggal As DateTime, ByVal kode_aset As String, ByVal qty As Decimal)
        Dim masuk, keluar As Decimal
        Dim nama_tabel As String
        Dim kolom(6), data(6) As String

        If qty > 0 Then masuk = qty
        If qty < 0 Then keluar = qty * -1

        nama_tabel = "kartu_persediaan"
        kolom(0) = "tipe"
        kolom(1) = "nomer_transaksi"
        kolom(2) = "tanggal"
        kolom(3) = "kode_aset"
        kolom(4) = "masuk"
        kolom(5) = "keluar"

        data(0) = tipe
        data(1) = nomer_transaksi
        data(2) = tanggal
        data(3) = kode_aset
        data(4) = masuk
        data(5) = keluar

        Return tambah_data(nama_tabel, kolom, data) & update_persediaan(kode_aset)
    End Function

    Private Function update_persediaan(ByVal kode_aset As String)
        Return "update ms_aset set persediaan = isnull((select sum(masuk-keluar) from kartu_persediaan where kode_aset = ms_aset.kode_aset),0);"
    End Function

    Private Function cek_persediaan(ByVal kode_aset As String) As Boolean
        Dim conn As New SqlConnection(strcon)
        Dim cmd As New SqlCommand
        Dim reader As SqlDataReader

        conn.Open()
        cmd = New SqlCommand("select * from ms_aset where kode_aset = '" & kode_aset & "' and persediaan > 0", conn)
        reader = cmd.ExecuteReader
        If reader.Read Then
            Return True
        Else
            Return False
        End If
    End Function
End Module
