﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLaporan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.gbTanggal = New System.Windows.Forms.GroupBox()
        Me.chkTanggal = New System.Windows.Forms.CheckBox()
        Me.dtSampai = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dtDari = New System.Windows.Forms.DateTimePicker()
        Me.gbKategori = New System.Windows.Forms.GroupBox()
        Me.cmbKategori = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.gbNoTrans = New System.Windows.Forms.GroupBox()
        Me.btnSearchNoTransaksi = New System.Windows.Forms.Button()
        Me.txtNoTrans = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnOk = New System.Windows.Forms.Button()
        Me.gbAset = New System.Windows.Forms.GroupBox()
        Me.lblNamaAset = New System.Windows.Forms.Label()
        Me.btnSearchAset = New System.Windows.Forms.Button()
        Me.txtKodeAset = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.gbTanggal.SuspendLayout()
        Me.gbKategori.SuspendLayout()
        Me.gbNoTrans.SuspendLayout()
        Me.gbAset.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label3.Location = New System.Drawing.Point(78, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(11, 13)
        Me.Label3.TabIndex = 58
        Me.Label3.Text = ":"
        '
        'gbTanggal
        '
        Me.gbTanggal.BackColor = System.Drawing.Color.Transparent
        Me.gbTanggal.Controls.Add(Me.chkTanggal)
        Me.gbTanggal.Controls.Add(Me.dtSampai)
        Me.gbTanggal.Controls.Add(Me.Label1)
        Me.gbTanggal.Controls.Add(Me.dtDari)
        Me.gbTanggal.Controls.Add(Me.Label3)
        Me.gbTanggal.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.gbTanggal.Location = New System.Drawing.Point(12, 6)
        Me.gbTanggal.Name = "gbTanggal"
        Me.gbTanggal.Size = New System.Drawing.Size(365, 38)
        Me.gbTanggal.TabIndex = 59
        Me.gbTanggal.TabStop = False
        '
        'chkTanggal
        '
        Me.chkTanggal.AutoSize = True
        Me.chkTanggal.Checked = True
        Me.chkTanggal.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkTanggal.Location = New System.Drawing.Point(6, 15)
        Me.chkTanggal.Name = "chkTanggal"
        Me.chkTanggal.Size = New System.Drawing.Size(65, 17)
        Me.chkTanggal.TabIndex = 125
        Me.chkTanggal.Text = "Tanggal"
        Me.chkTanggal.UseVisualStyleBackColor = True
        '
        'dtSampai
        '
        Me.dtSampai.CustomFormat = "dd MMM yyyy"
        Me.dtSampai.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtSampai.Location = New System.Drawing.Point(234, 12)
        Me.dtSampai.Name = "dtSampai"
        Me.dtSampai.Size = New System.Drawing.Size(105, 20)
        Me.dtSampai.TabIndex = 61
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label1.Location = New System.Drawing.Point(206, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(22, 13)
        Me.Label1.TabIndex = 60
        Me.Label1.Text = "s/d"
        '
        'dtDari
        '
        Me.dtDari.CustomFormat = "dd MMM yyyy"
        Me.dtDari.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtDari.Location = New System.Drawing.Point(95, 12)
        Me.dtDari.Name = "dtDari"
        Me.dtDari.Size = New System.Drawing.Size(105, 20)
        Me.dtDari.TabIndex = 59
        '
        'gbKategori
        '
        Me.gbKategori.BackColor = System.Drawing.Color.Transparent
        Me.gbKategori.Controls.Add(Me.cmbKategori)
        Me.gbKategori.Controls.Add(Me.Label2)
        Me.gbKategori.Controls.Add(Me.Label5)
        Me.gbKategori.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.gbKategori.Location = New System.Drawing.Point(12, 140)
        Me.gbKategori.Name = "gbKategori"
        Me.gbKategori.Size = New System.Drawing.Size(239, 39)
        Me.gbKategori.TabIndex = 60
        Me.gbKategori.TabStop = False
        Me.gbKategori.Visible = False
        '
        'cmbKategori
        '
        Me.cmbKategori.FormattingEnabled = True
        Me.cmbKategori.Location = New System.Drawing.Point(95, 13)
        Me.cmbKategori.Name = "cmbKategori"
        Me.cmbKategori.Size = New System.Drawing.Size(121, 21)
        Me.cmbKategori.TabIndex = 61
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label2.Location = New System.Drawing.Point(6, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(47, 13)
        Me.Label2.TabIndex = 59
        Me.Label2.Text = "Kategori"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label5.Location = New System.Drawing.Point(78, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(11, 13)
        Me.Label5.TabIndex = 60
        Me.Label5.Text = ":"
        '
        'gbNoTrans
        '
        Me.gbNoTrans.BackColor = System.Drawing.Color.Transparent
        Me.gbNoTrans.Controls.Add(Me.btnSearchNoTransaksi)
        Me.gbNoTrans.Controls.Add(Me.txtNoTrans)
        Me.gbNoTrans.Controls.Add(Me.Label10)
        Me.gbNoTrans.Controls.Add(Me.Label11)
        Me.gbNoTrans.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.gbNoTrans.Location = New System.Drawing.Point(12, 50)
        Me.gbNoTrans.Name = "gbNoTrans"
        Me.gbNoTrans.Size = New System.Drawing.Size(310, 39)
        Me.gbNoTrans.TabIndex = 119
        Me.gbNoTrans.TabStop = False
        Me.gbNoTrans.Visible = False
        '
        'btnSearchNoTransaksi
        '
        Me.btnSearchNoTransaksi.Location = New System.Drawing.Point(222, 11)
        Me.btnSearchNoTransaksi.Name = "btnSearchNoTransaksi"
        Me.btnSearchNoTransaksi.Size = New System.Drawing.Size(75, 23)
        Me.btnSearchNoTransaksi.TabIndex = 117
        Me.btnSearchNoTransaksi.Text = "Search"
        Me.btnSearchNoTransaksi.UseVisualStyleBackColor = True
        '
        'txtNoTrans
        '
        Me.txtNoTrans.Location = New System.Drawing.Point(95, 13)
        Me.txtNoTrans.Name = "txtNoTrans"
        Me.txtNoTrans.Size = New System.Drawing.Size(121, 20)
        Me.txtNoTrans.TabIndex = 61
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label10.Location = New System.Drawing.Point(6, 16)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(72, 13)
        Me.Label10.TabIndex = 59
        Me.Label10.Text = "No. Transaksi"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label11.Location = New System.Drawing.Point(78, 16)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(11, 13)
        Me.Label11.TabIndex = 60
        Me.Label11.Text = ":"
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.Transparent
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnCancel.Location = New System.Drawing.Point(288, 260)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(98, 31)
        Me.btnCancel.TabIndex = 121
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnOk
        '
        Me.btnOk.BackColor = System.Drawing.Color.Transparent
        Me.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnOk.FlatAppearance.BorderSize = 0
        Me.btnOk.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnOk.Location = New System.Drawing.Point(184, 260)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(98, 31)
        Me.btnOk.TabIndex = 120
        Me.btnOk.Text = "Print"
        Me.btnOk.UseVisualStyleBackColor = False
        '
        'gbAset
        '
        Me.gbAset.BackColor = System.Drawing.Color.Transparent
        Me.gbAset.Controls.Add(Me.lblNamaAset)
        Me.gbAset.Controls.Add(Me.btnSearchAset)
        Me.gbAset.Controls.Add(Me.txtKodeAset)
        Me.gbAset.Controls.Add(Me.Label14)
        Me.gbAset.Controls.Add(Me.Label15)
        Me.gbAset.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.gbAset.Location = New System.Drawing.Point(12, 95)
        Me.gbAset.Name = "gbAset"
        Me.gbAset.Size = New System.Drawing.Size(476, 39)
        Me.gbAset.TabIndex = 123
        Me.gbAset.TabStop = False
        Me.gbAset.Visible = False
        '
        'lblNamaAset
        '
        Me.lblNamaAset.AutoSize = True
        Me.lblNamaAset.BackColor = System.Drawing.Color.Transparent
        Me.lblNamaAset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNamaAset.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblNamaAset.Location = New System.Drawing.Point(303, 16)
        Me.lblNamaAset.Name = "lblNamaAset"
        Me.lblNamaAset.Size = New System.Drawing.Size(11, 13)
        Me.lblNamaAset.TabIndex = 118
        Me.lblNamaAset.Text = "-"
        '
        'btnSearchAset
        '
        Me.btnSearchAset.Location = New System.Drawing.Point(222, 11)
        Me.btnSearchAset.Name = "btnSearchAset"
        Me.btnSearchAset.Size = New System.Drawing.Size(75, 23)
        Me.btnSearchAset.TabIndex = 117
        Me.btnSearchAset.Text = "Search"
        Me.btnSearchAset.UseVisualStyleBackColor = True
        '
        'txtKodeAset
        '
        Me.txtKodeAset.Location = New System.Drawing.Point(95, 13)
        Me.txtKodeAset.Name = "txtKodeAset"
        Me.txtKodeAset.Size = New System.Drawing.Size(121, 20)
        Me.txtKodeAset.TabIndex = 61
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label14.Location = New System.Drawing.Point(6, 16)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(56, 13)
        Me.Label14.TabIndex = 59
        Me.Label14.Text = "Kode Aset"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label15.Location = New System.Drawing.Point(78, 16)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(11, 13)
        Me.Label15.TabIndex = 60
        Me.Label15.Text = ":"
        '
        'frmLaporan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(593, 319)
        Me.Controls.Add(Me.gbAset)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOk)
        Me.Controls.Add(Me.gbNoTrans)
        Me.Controls.Add(Me.gbKategori)
        Me.Controls.Add(Me.gbTanggal)
        Me.DoubleBuffered = True
        Me.Name = "frmLaporan"
        Me.Text = "frmLaporan"
        Me.gbTanggal.ResumeLayout(False)
        Me.gbTanggal.PerformLayout()
        Me.gbKategori.ResumeLayout(False)
        Me.gbKategori.PerformLayout()
        Me.gbNoTrans.ResumeLayout(False)
        Me.gbNoTrans.PerformLayout()
        Me.gbAset.ResumeLayout(False)
        Me.gbAset.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents gbTanggal As System.Windows.Forms.GroupBox
    Friend WithEvents dtSampai As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtDari As System.Windows.Forms.DateTimePicker
    Friend WithEvents gbKategori As System.Windows.Forms.GroupBox
    Friend WithEvents cmbKategori As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents gbNoTrans As System.Windows.Forms.GroupBox
    Friend WithEvents btnSearchNoTransaksi As System.Windows.Forms.Button
    Friend WithEvents txtNoTrans As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnOk As System.Windows.Forms.Button
    Friend WithEvents gbAset As System.Windows.Forms.GroupBox
    Friend WithEvents lblNamaAset As System.Windows.Forms.Label
    Friend WithEvents btnSearchAset As System.Windows.Forms.Button
    Friend WithEvents txtKodeAset As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents chkTanggal As System.Windows.Forms.CheckBox
End Class
